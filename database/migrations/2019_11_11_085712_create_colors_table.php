<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colors', function (Blueprint $table) {
            $table->uuid('id')
                ->primary();
            $table->string('color');
            $table->uuid('colorer_id');
            $table->string('colorer_type')->nullable();
            $table->index(["colorer_id", "colorer_type"]);
            $table->string("colorable_type");
            $table->uuid("colorable_id");
            $table->index(["colorable_id", "colorable_type"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colors');
    }
}
