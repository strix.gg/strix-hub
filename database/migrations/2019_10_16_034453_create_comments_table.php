<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->uuid('id')
            ->primary();
            $table->longText('content');
            $table->uuid('commenter_id');
            $table->string('commenter_type')->nullable();
            $table->index(["commenter_id", "commenter_type"]);
            $table->string("commentable_type");
            $table->uuid("commentable_id");
            $table->index(["commentable_type", "commentable_id"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
