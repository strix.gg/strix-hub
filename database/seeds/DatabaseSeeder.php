<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);


        // only call this when u actually need to put credits in.
        $this->call(CreditPackageSeeder::class);

        $this->call(PermissionRoleSeeder::class);
    }
}
