<?php

use Illuminate\Database\Seeder;
use App\Models\Store\Product;

class CreditPackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'title' => 'Pile of Crowns',
            'base_amount' => 120,
            'bonus_amount' => 0,
            'price' => 1.99 * 100
        ]);

        Product::create([
            'title' => 'Backpack of Crowns',
            'base_amount' => 450,
            'bonus_amount' => 0,
            'price' => 7.49 * 100
        ]);

        Product::create([
            'title' => 'Chest of Crowns',
            'base_amount' => 1800,
            'bonus_amount' => 100,
            'price' => 29.99 * 100
        ]);

        Product::create([
            'title' => 'Trunk of Crowns',
            'base_amount' => 4500,
            'bonus_amount' => 450,
            'price' => 74.99 * 100
        ]);

        Product::create([
            'title' => 'Trailer of Crowns',
            'base_amount' => 15000,
            'bonus_amount' => 2250,
            'price' => 249.49 * 100
        ]);

        Product::create([
            'title' => 'Truckload of Crowns',
            'base_amount' => 30000,
            'bonus_amount' => 6000,
            'price' => 499.99 * 100
        ]);
    }
}
