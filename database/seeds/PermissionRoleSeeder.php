<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // user permissions
        Permission::create(['name' => 'view user']);
        Permission::create(['name' => 'create user']);
        Permission::create(['name' => 'edit user']);
        Permission::create(['name' => 'update user']);
        Permission::create(['name' => 'delete user']);

        /*
         * Forum Permissions
         *
         */

        // category permissions
        Permission::create(['name' => 'view category']);
        Permission::create(['name' => 'create category']);
        Permission::create(['name' => 'edit category']);
        Permission::create(['name' => 'update category']);
        Permission::create(['name' => 'delete category']);

        // board permissions
        Permission::create(['name' => 'view board']);
        Permission::create(['name' => 'create board']);
        Permission::create(['name' => 'edit board']);
        Permission::create(['name' => 'update board']);
        Permission::create(['name' => 'delete board']);

        // thread permissions
        Permission::create(['name' => 'view thread']);
        Permission::create(['name' => 'create thread']);
        Permission::create(['name' => 'edit thread']);
        Permission::create(['name' => 'update thread']);
        Permission::create(['name' => 'delete thread']);

        // comment permissions
        Permission::create(['name' => 'view comment']);
        Permission::create(['name' => 'create comment']);
        Permission::create(['name' => 'edit comment']);
        Permission::create(['name' => 'update comment']);
        Permission::create(['name' => 'delete comment']);

        /*
         * Store Permissions
         *
         */

        Permission::create(['name' => 'view credit']);
        Permission::create(['name' => 'create credit']);
        Permission::create(['name' => 'edit credit']);
        Permission::create(['name' => 'update credit']);
        Permission::create(['name' => 'delete credit']);

        Permission::create(['name' => 'view sale']);
        Permission::create(['name' => 'create sale']);
        Permission::create(['name' => 'edit sale']);
        Permission::create(['name' => 'update sale']);
        Permission::create(['name' => 'delete sale']);

        Role::create(['name' => 'user'])
            ->givePermissionTo([
                'view user',
                'view category',
                'view board',
                'view thread',
                'create thread',
                'view comment',
                'create comment'
            ]);

        Role::create(['name' => 'vip'])
            ->givePermissionTo([
                'view user',
                'view category',
                'view board',
                'view thread',
                'create thread',
                'view comment',
                'create comment'
            ]);

        Role::create(['name' => 'gold vip'])
            ->givePermissionTo([
                'view user',
                'view category',
                'view board',
                'view thread',
                'create thread',
                'view comment',
                'create comment'
            ]);

        Role::create(['name' => 'knight'])
            ->givePermissionTo([
                'view user',
                'view category',
                'view board',
                'view thread',
                'create thread',
                'view comment',
                'create comment'
            ]);

        Role::create(['name' => 'king'])
            ->givePermissionTo([
                'view user',
                'view category',
                'view board',
                'view thread',
                'create thread',
                'view comment',
                'create comment'
            ]);

        Role::create(['name' => 'staff'])
            ->givePermissionTo([
                'view user',
                'view category',
                'view board',
                'view thread',
                'create thread',
                'edit thread',
                'delete thread',
                'view comment',
                'edit comment',
                'delete comment',
                'create comment'
            ]);

        Role::create(['name' => 'owner'])
            ->givePermissionTo(Permission::all());
    }
}
