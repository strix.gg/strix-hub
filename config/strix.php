<?php

return [
    'theme' => env('THEME_NAME', 'Strix'),
    'login_traditional' => false,
    'login_steam' => true,
    'version' => '0.6.0',
    'discord_url' => env('DISCORD_LINK', 'https://discord.gg/PtqzEYW'),
    'steam_url' => env('STEAM_GROUP_LINK', 'https://steamcommunity.com/groups/KingsgamingRP'),
    'staff_roles' => [
        'owner', 'staff'
    ],
    'premium_roles' => [
        'vip', 'gold vip', 'knight', 'king'
    ],
];
