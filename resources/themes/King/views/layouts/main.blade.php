<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>
    <meta name="title" content="Kings Gaming"/>
    <meta name="description" content="Let's cut to the chase, you want to play, we got content. Let's show you what we're made of."/>
    <meta name="keywords"
          content="Darkrp, Gmod, UK gmod, US Gmod, UK darkrp server, US darkrp server "/>
    <meta name="robots" content="index, follow"/>
    <meta name="language" content="English"/>
    <meta name="revisit-after" content="3 days"/>
    <meta name="author" content="Kings Gaming" />
    <meta property="og:site_name" content="Kings Gaming" />
    <meta property="og:title" content="Kings Gaming"/>
    <meta property="og:description"
          content="Let's cut to the chase, you want to play, we got content. Let's show you what we're made of."/>
    <meta property="og:url" content="https://kingsgaming.us"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    {!! Theme::css('css/app.css?v={cache-version}') !!}
</head>

<body id="body" class="scrollable" style="@yield('body_class')">

<div class="font-sans font-normal antialiased">
    <div class="relative h-full" id="app">
        @include('partials.header')

        @yield('content')

        @include('partials.modals.store')

        <changelog-modal v-cloak signature="{{ \Cache::get('version_signature') }}"></changelog-modal>
    </div>
</div>

<!-- Scripts -->
{!! Theme::js('js/app.js?v={cache-version}') !!}

</body>
</html>
