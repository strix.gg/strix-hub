<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }} - Admin</title>

    <!-- Styles -->
    {!! Theme::css('css/app.css?v={cache-version}') !!}
</head>

<body id="body" class="scrollable fixed w-full" style="@yield('body_class')">

<div class="font-sans font-normal antialiased">
    <div class="relative h-full" id="app">

        <div class="flex">
            <div class="flex flex-col flex-none bg-neutral-600 min-h-screen w-56 h-screen">
                <ul class="list-reset flex flex-col text-center md:text-left overflow-hidden" role="tablist">
                    <li class="overflow-hidden py-2 pl-3 text-neutral-300">Users</li>

                    <li class="flex-1 bg-transparent">
                        <div class="flex flex-none items-center align-middle text-neutral-100">
                            <a
                                href="{{ route('admin.users.index') }}"
                                class="ml-2 pl-3 py-1 text-sm focus:outline-none w-full text-left">Users</a>
                        </div>
                    </li>
                </ul>

                <ul class="list-reset flex flex-col text-center md:text-left overflow-hidden" role="tablist">
                    <li class="overflow-hidden py-2 pl-3 text-neutral-300">Forums</li>

                    <li class="flex-1 bg-transparent">
                        <div class="flex flex-none items-center align-middle text-neutral-100">
                            <a
                                href="{{ route('admin.category.index') }}"
                                class="ml-2 pl-3 py-1 text-sm focus:outline-none w-full text-left">Category</a>
                        </div>
                    </li>
                    <li class="flex-1 bg-transparent">
                        <div class="flex flex-none items-center align-middle text-neutral-100">
                            <a
                                href="{{ route('admin.board.index') }}"
                                class="ml-2 pl-3 py-1 text-sm focus:outline-none w-full text-left">Boards</a>
                        </div>
                    </li>
                </ul>
                <ul class="list-reset flex flex-col text-center md:text-left overflow-hidden" role="tablist">
                    <li class="overflow-hidden py-2 pl-3 text-neutral-300">Roles & Permissions</li>

                    <li class="flex-1 bg-transparent">
                        <div class="flex flex-none items-center align-middle text-neutral-100">
                            <a
                                href="{{ route('admin.role.index') }}"
                                class="ml-2 pl-3 py-1 text-sm focus:outline-none w-full text-left">Roles</a>
                        </div>
                    </li>
                </ul>

                <ul class="list-reset flex flex-col text-center md:text-left overflow-hidden" role="tablist">
                    <li class="overflow-hidden py-2 pl-3 text-neutral-300">News & Updates</li>

                    <li class="flex-1 bg-transparent">
                        <div class="flex flex-none items-center align-middle text-neutral-100">
                            <a
                                href="{{ route('admin.changelog.index') }}"
                                class="ml-2 pl-3 py-1 text-sm focus:outline-none w-full text-left">Changelogs</a>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="flex flex-col min-h-screen h-screen w-full max-w-full">
                <div class="flex overflow-y-auto relative w-full">
                    <div class="p-12 w-full flex flex-wrap w-full">
                        @include('partials.errors')
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Scripts -->
{!! Theme::js('js/app.js?v={cache-version}') !!}
</body>
</html>
