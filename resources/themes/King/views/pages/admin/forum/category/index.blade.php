@extends('layouts.admin')

@section('content')
    <div class="w-1/3">
        {{ $categories->links() }}
        <div class="ml-auto">
            <a class="button button-primary" href="{{ route('admin.category.create') }}">
                Create Category
            </a>
        </div>

        @foreach($categories as $category)
            <div class="w-full">
                <div class="bg-neutral-500 flex flex-row items-center p-3">
                    <div class="text-white font-display-1 text-lg">
                        {{ $category->title }}
                    </div>

                    <div class="ml-auto">
                        <a class="button button-secondary" href="{{ route('admin.category.show', $category) }}">
                            Edit
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
