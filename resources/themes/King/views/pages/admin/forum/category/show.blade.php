@extends('layouts.admin')

@section('content')
    <form method="POST" class="w-full md:w-1/3" action="{{ route('admin.category.update', [$category]) }}">
        @csrf
        @method('PATCH')
        <div class="w-full">
            <div class="flex flex-wrap">
                <div class="w-full my-2 p-3">
                    <label class="form-input-container">
                        <input id="name" type="text" name="title"
                               class="form-input form-input-primary @error('title') border-red-500 @enderror"
                               value="{{ old('title') ?? $category->title }}"
                               placeholder=" "
                               required autocomplete="title">
                        <div class="form-input-label">Title</div>
                    </label>

                    @error('title')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

                <div class="w-full my-2 p-3">
                    <label class="form-input-container">
                        <input id="name" type="text" name="description"
                               class="form-input form-input-primary @error('description') border-red-500 @enderror"
                               value="{{ old('description') ?? $category->description }}"
                               placeholder=" "
                               autocomplete="description">
                        <div class="form-input-label">Description</div>
                    </label>

                    @error('title')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

                <div class="w-full p-3 text-left">
                    <div class="flex flex-col">
                        <button type="submit" class="button button-primary button-hero mx-0 my-2">Update Category</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form method="POST" class="w-full md:w-1/3" enctype="multipart/form-data" action="{{ route('admin.category.media.update', [$category]) }}">
        @csrf
        @method('PATCH')
        <div class="w-full">
            <div class="w-full p-3 mt-3">
                <div class="block">
                    <div class="h-48 w-160 rounded px-4 py-6 bg-neutral-500 rounded border border-neutral-400 @error('cover') border-red-500 @enderror text-neutral-500"
                         style="background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url({{ $category->getFirstMediaUrl('cover') }})">
                    </div>

                    <div class="flex flex-col">
                        <span class="text-neutral-100 mt-2 text-base leading-normal">Upload Cover</span>
                        <span class="text-neutral-200">We recommend a wide image, at least 1420x420px. This banner will be displayed on the background of your user profile.</span>

                        <label class="button button-secondary button-hero text-neutral-100 mx-0 m-2">
                            <input name="cover" type='file' class="hidden" />
                            <span>Upload Cover</span>
                        </label>
                    </div>
                </div>

                @error('cover')
                <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                @enderror
            </div>
        </div>

        <div class="w-full p-3 text-left">
            <div class="flex flex-col mt-3">
                <button type="submit" class="button button-primary button-hero mx-0 my-2">Update Media</button>
            </div>
        </div>
    </form>
@endsection
