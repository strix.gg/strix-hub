@extends('layouts.admin')

@section('content')
    <div class="w-1/3">
        {{ $boards->links() }}
        <div class="ml-auto">
            <a class="button button-primary" href="{{ route('admin.board.create') }}">
                Create Board
            </a>
        </div>

        @foreach($boards as $board)
            <div class="w-full">
                <div class="bg-neutral-500 flex flex-row items-center p-3">
                    <div class="text-white font-display-1 text-lg">
                        {{ $board->title }}
                    </div>

                    <div class="ml-auto">
                        <a class="button button-secondary" href="{{ route('admin.board.show', $board) }}">
                            Edit
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
