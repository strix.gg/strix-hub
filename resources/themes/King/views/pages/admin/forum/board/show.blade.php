@extends('layouts.admin')

@section('content')
    <form method="POST" class="w-full md:w-1/3" action="{{ route('admin.board.update', $board) }}">
        @csrf
        @method('PATCH')
        <div class="max-w-full">
            <div class="flex flex-wrap">
                <div class="w-full my-2 p-3">
                    <label class="form-input-container">
                        <input id="title" type="text" name="title"
                               class="form-input form-input-primary @error('title') border-red-500 @enderror"
                               value="{{ $board->title ?? old('title') }}"
                               placeholder=" "
                               required autocomplete="title">
                        <div class="form-input-label">Title</div>
                    </label>

                    @error('title')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

                <div class="w-full my-2 p-3">
                    <label class="form-input-container">
                        <input id="description" type="text" name="description"
                               class="form-input form-input-primary @error('description') border-red-500 @enderror"
                               value="{{ $board->description ?? old('description') }}"
                               placeholder=" "
                               autocomplete="description">
                        <div class="form-input-label">Description</div>
                    </label>

                    @error('description')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

                <div class="w-full my-2 p-3">
                    <label class="block">
                        <span class="text-gray-700">Categories</span>
                        <select name="category_id" class="form-select mt-1 block w-full">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                    </label>

                    @error('description')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

                <div class="w-full p-3 text-left">
                    <div class="flex flex-col">
                        <button type="submit" class="button button-primary button-hero mx-0 my-2">Edit Board</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form method="POST" class="w-full md:w-2/3" enctype="multipart/form-data" action="{{ route('admin.board.media.update', [$board]) }}">
        @csrf
        @method('PATCH')
        <div class="w-full p-3">
            <div class="flex items-center">
                <div class="h-24 w-24 rounded-full px-4 py-6 bg-neutral-500 rounded border border-neutral-400 @error('icon') border-red-500 @enderror text-neutral-500"
                     style="background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url({{ $board->getFirstMediaUrl('icon') }})">
                </div>

                <div class="flex flex-col ml-5">
                    <span class="text-neutral-100 mt-2 text-base leading-normal">Upload Icon</span>
                    <span class="text-neutral-200">We recommend a square image at least 256x256px.</span>

                    <label class="button button-secondary button-hero h-auto text-neutral-100 mx-0 m-2">
                        <input name="icon" type='file' class="hidden" />
                        <span>Upload Icon</span>
                    </label>
                </div>
            </div>

            @error('icon')
            <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
            @enderror
        </div>

        <div class="w-full p-3 text-left">
            <div class="flex flex-col mt-3">
                <button type="submit" class="button button-primary button-hero mx-0 my-2">Update Media</button>
            </div>
        </div>
    </form>
@endsection
