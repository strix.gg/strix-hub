@extends('layouts.admin')

@section('content')
    <form method="POST" class="w-full" action="{{ route('admin.board.store') }}">
        @csrf
        <div class="w-1/3">
            <div class="flex flex-wrap">
                <div class="w-full my-2 p-3">
                    <label class="form-input-container">
                        <input id="title" type="text" name="title"
                               class="form-input form-input-primary @error('title') border-red-500 @enderror"
                               value="{{ old('title') }}"
                               placeholder=" "
                               required autocomplete="title">
                        <div class="form-input-label">Title</div>
                    </label>

                    @error('title')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

                <div class="w-full my-2 p-3">
                    <label class="form-input-container">
                        <input id="description" type="text" name="description"
                               class="form-input form-input-primary @error('description') border-red-500 @enderror"
                               value="{{ old('description') }}"
                               placeholder=" "
                               autocomplete="description">
                        <div class="form-input-label">Description</div>
                    </label>

                    @error('description')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

                <div class="w-full my-2 p-3">
                    <label class="block">
                        <span class="text-gray-700">Categories</span>
                        <select name="category_id" class="form-select mt-1 block w-full">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                    </label>

                    @error('description')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

                <div class="w-full p-3 text-left">
                    <div class="flex flex-col">
                        <button type="submit" class="button button-primary button-hero mx-0 my-2">Create Board</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
