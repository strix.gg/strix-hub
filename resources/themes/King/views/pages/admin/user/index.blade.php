@extends('layouts.admin')

@section('content')
    <div class="w-1/3">
        {{ $users->links() }}
        @foreach($users as $user)
            <div class="w-full">
                <div class="bg-neutral-500 flex flex-row items-center p-3">
                    <div class="text-white font-display-1 text-lg">
                        {{ $user->name }}
                    </div>

                    <div class="ml-auto">
                        <a class="button button-secondary" href="{{ route('admin.users.show', $user) }}">
                            Edit
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
