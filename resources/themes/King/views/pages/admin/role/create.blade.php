@extends('layouts.admin')

@section('content')
    <form method="POST" class="w-full" action="{{ route('admin.role.store') }}">
        @csrf
        <div class="w-1/3">
            <div class="flex flex-wrap">
                <div class="w-full my-2 p-3">
                    <label class="form-input-container">
                        <input id="title" type="text" name="name"
                               class="form-input form-input-primary @error('name') border-red-500 @enderror"
                               value="{{ old('name') }}"
                               placeholder=" "
                               required>
                        <div class="form-input-label">Name</div>
                    </label>

                    @error('name')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

                <div class="w-full my-2 p-3">
                    <label class="form-input-container">
                        <input id="title" type="text" name="guard_name"
                               class="form-input form-input-primary @error('guard_name') border-red-500 @enderror"
                               value="{{ old('guard_name') }}"
                               placeholder=" "
                               required>
                        <div class="form-input-label">Guard Name</div>
                    </label>

                    @error('guard_name')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

            </div>

            <div class="w-full p-3 text-left">
                <div class="flex flex-col">
                    <button type="submit" class="button button-primary button-hero mx-0 my-2">Create Role</button>
                </div>
            </div>
        </div>
    </form>
@endsection
