@extends('layouts.admin')

@section('content')
    <form method="POST" class="w-full md:w-1/3" action="{{ route('admin.role.update', $role) }}">
        @csrf
        @method('PATCH')

        <div class="w-full">
            <div class="flex flex-wrap">
                <div class="w-full my-2 p-3">
                    <label class="form-input-container">
                        <input id="title" type="text" name="name"
                               class="form-input form-input-primary @error('name') border-red-500 @enderror"
                               value="{{ $role->name ?? old('name') }}"
                               placeholder=" "
                               required>
                        <div class="form-input-label">Name</div>
                    </label>

                    @error('name')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

                <div class="w-full my-2 p-3">
                    <label class="form-input-container">
                        <input id="title" type="text" name="guard_name"
                               class="form-input form-input-primary @error('guard_name') border-red-500 @enderror"
                               value="{{ $role->guard_name ?? old('guard_name') }}"
                               placeholder=" "
                               required>
                        <div class="form-input-label">Guard Name</div>
                    </label>

                    @error('guard_name')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

            </div>

            <div class="w-full p-3 text-left">
                <div class="flex flex-col">
                    <button type="submit" class="button button-primary button-hero mx-0 my-2">Update Role</button>
                </div>
            </div>
        </div>
    </form>

    <form method="POST" class="w-full md:w-1/3" enctype="multipart/form-data" action="{{ route('admin.role.media.update', $role) }}">
        @csrf
        @method('PATCH')

        <div class="w-full">
            <div class="flex flex-wrap">

                <div class="w-full p-3 mt-3">
                    <div class="block">
                        <div class="h-48 w-75 rounded px-4 py-6 bg-neutral-500 rounded border border-neutral-400 @error('userbar') border-red-500 @enderror text-neutral-500"
                             style="background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url({{ $role->getFirstMediaUrl('userbar') }})">
                        </div>

                        <div class="flex flex-col">
                            <span class="text-neutral-100 mt-2 text-base leading-normal">Upload Cover</span>
                            <span class="text-neutral-200">We recommend a wide image, at least ???px. This image will display as the main media for the role.</span>

                            <label class="button button-secondary button-hero text-neutral-100 mx-0 m-2">
                                <input name="userbar" type='file' class="hidden" />
                                <span>Upload Userbar</span>
                            </label>
                        </div>
                    </div>

                    @error('userbar')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>


                <div class="w-full p-3 text-left">
                    <div class="flex flex-col mt-3">
                        <button type="submit" class="button button-primary button-hero mx-0 my-2">Update Media</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
