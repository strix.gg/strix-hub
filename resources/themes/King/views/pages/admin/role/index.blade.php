@extends('layouts.admin')

@section('content')
    <div class="w-1/3">

        {{ $roles->links() }}

        <div class="ml-auto">
            <a class="button button-primary mb-4" href="{{ route('admin.role.create') }}">
                Create Roles
            </a>
        </div>

        @foreach($roles as $role)
            <div class="w-full">
                <div class="bg-neutral-500 flex flex-row items-center p-3">
                    <div class="text-white font-display-1 text-lg">
                        {{ $role->name }}
                    </div>

                    <div class="ml-auto">
                        <a class="button button-secondary" href="{{ route('admin.role.show', $role) }}">
                            Edit
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
