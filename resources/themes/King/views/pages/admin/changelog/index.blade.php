@extends('layouts.admin')

@section('content')
    <div class="w-1/3">
        {{ $changelogs->links() }}
        <div class="ml-auto">
            <a class="button button-primary" href="{{ route('admin.changelog.create') }}">
                Create Changelog
            </a>
        </div>

        @foreach($changelogs as $changelog)
            <div class="w-full">
                <div class="bg-neutral-500 flex flex-row items-center p-3">
                    <div class="text-white font-display-1 text-lg">
                        {{ $changelog->title }}
                    </div>

                    <div class="ml-auto">
                        <a class="button button-secondary" href="{{ route('admin.changelog.show', $changelog) }}">
                            Edit
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
