@extends('layouts.admin')

@section('content')
    <form method="POST" class="w-full" action="{{ route('admin.changelog.store') }}">
        @csrf
        <div class="w-1/3">
            <div class="flex flex-wrap">

                <div class="bg-neutral-500 rounded text-neutral-200">
                    <label class="form-input-container">
                        <input id="title" type="text" name="title"
                               class="form-input rounded-b-none border-0 p-4"
                               value="{{ old('title') }}"
                               placeholder="Title"
                               required autocomplete="title">
                    </label>

                    @error('title')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <editor-component editor_classes="rounded-t-none" editor_classes=""></editor-component>
                </div>


                <div class="w-full p-3 text-left">
                    <div class="flex flex-col">
                        <button type="submit" class="button button-primary button-hero mx-0 my-2">Create Changelog</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
