@extends('layouts.main')

@section('content')
    <main class="block">

        @include('partials.spacer')

        <section class="index-section">
            <div class='container index-container'>

                @include('partials.store')

                <div class="index-hero">
                    <h1 class="index-hero__title">Welcome to Kings Gaming</h1>

                    <p class="index-hero__sub-title">
                        Let's cut to the chase, you want to play, we got content. Let's show you what we're made of.
                    </p>

                    <div class="index-hero__connect-container">
                        <a href="steam://connect/144.217.11.219:27015" class="button button-primary button-hero">
                            Connect
                        </a>
                    </div>

                    <div class="index-hero__button-container">
                        <div class="w-1/3 p-2">
                            <a class="button button-secondary h-10 w-full" href="{{ route('redirects.discord') }}">
                                <i class="fab fa-discord fa-fw mr-2"></i>
                                <span>Discord</span>
                            </a>
                        </div>

                        <div class="w-1/3 p-2">
                            <a class="button button-secondary h-10 w-full" @click="$modal.show('store-modal')">
                                <i class="fad fa-store fa-fw mr-2"></i>
                                <span>Store</span>
                            </a>
                        </div>


                        <div class="w-1/3 p-2">
                            <a class="button button-secondary h-10 w-full" href="{{ route('redirects.steam.group') }}">
                                <i class="fab fa-steam fa-fw mr-2"></i>
                                <span>Steam</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
