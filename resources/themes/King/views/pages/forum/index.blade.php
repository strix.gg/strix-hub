@extends('layouts.main')

@section('content')
    <main class="block">
        @include('partials.spacer')
        <section class="forum-section">
            <div class="container forum-container">
                <div class="w-full md:w-3/4 px-2">
                    @foreach($categories as $category)
                        <div class="card m-1 w-full">
                            <div class="card-header">
                                <div class="card-header-image-container">
                                    <div class="lazy card-header-image" data-bg="url({{ $category->getMediaUrl('cover') }})"></div>
                                </div>
                                <div class="z-10 text-white">
                                    <span class="font-display-1 text-lg">{{ $category->title }}</span>
                                </div>
                            </div>

                            <div class="card-content forum-category-content">
                                @foreach($category->boards as $board)
                                    <div class="card-grid-item forum-category-body">

                                        <div class="forum-board">
                                            <div class="forum-board-icon">
                                                <a href="{{ route('forums.board.show', $board) }}">
                                                    <img class="lazy" data-src="{{ $board->getFirstMediaUrl('icon') }}" alt="">
                                                </a>
                                            </div>
                                            <div class="forum-board-info">
                                                <h3 class="forum-board-info-title">
                                                    <a href="{{ route('forums.board.show', $board) }}">{{ $board->title }}</a>
                                                </h3>
                                                <span class="text-neutral-200">Threads:</span>
                                                {{ $board->thread_count }}
                                                <span class="text-neutral-200">Comments:</span>
                                                {{ $board->comment_count }}
                                            </div>
                                            @if($board->threads()->exists())
                                                <div class="forum-board-last-thread">
                                                    <div class="truncate z-10">
                                                        <a class="text-primary-500" href="{{ route('forums.thread.show', [$board->threads->last(), $board->threads->last()->slug]) }}">
                                                            {{ $board->threads->last()->title }}
                                                        </a>
                                                    </div>
                                                    <div class="truncate z-10">
                                                        <div class="inline-block">
                                                            <span>{{ $board->threads->last()->created_at->diffForHumans() }}</span>
                                                        </div>
                                                        |
                                                        <a class="text-primary-500" href="{{ route('users.show', $board->threads->last()->user) }}" style="color: {{ $board->threads->last()->user->getColor() }} !important;">
                                                            {{ $board->threads->last()->user->name }}
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="forum-board-last-thread-avatar">
                                                    <a href="{{ route('users.show', $board->threads->last()->user) }}">
                                                        <div>
                                                            <user-hovercard-component slug="{{ $board->threads->last()->user->slug }}">
                                                                <img class="lazy" data-src="{{ $board->threads->last()->user->getMediaUrl('avatar') }}" alt="">
                                                            </user-hovercard-component>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endif
                                        </div>

                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="w-full md:w-1/4 px-2">
                    <div class="card m-1 w-full">

                        <div class="card-header">
                            <div class="z-10 text-white">
                                <span class="font-display-1 text-lg">Latest Threads</span>
                            </div>
                        </div>

                        <div class="card-content items-start justify-start">
                            @foreach($threads as $thread)
                                <div class="card-grid-item forum-widget-body">
                                    <div class="forum-widget">
                                        <div class="forum-widget-icon">
                                            <a href="{{ route('users.show', $thread->user) }}">
                                                <div>
                                                    <user-hovercard-component slug="{{ $thread->user->slug }}">
                                                        <img class="lazy" data-src="{{ $thread->user->getMediaUrl('avatar') }}" alt="">
                                                    </user-hovercard-component>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="forum-widget-info-title">
                                            <h3>
                                                <a href="{{ route('forums.thread.show', [$thread, $thread->slug]) }}">
                                                    {{ $thread->title }}
                                                </a>
                                            </h3>
                                            <div class="forum-widget-info-details">
                                                <span>{{ $thread->created_at->diffForHumans() }}</span>
                                                by
                                                <span>
                                                    <a class="text-primary-500" href="{{ route('users.show', $thread->user) }}" style="color: {{ $thread->user->getColor() }} !important;">
                                                        {{ $thread->user->name }}
                                                    </a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>
                </div>

            </div>
        </section>
    </main>
@endsection
