@extends('layouts.main')

@section('content')
    <main class="block">
        @include('partials.spacer')
        <section class="relative flex flex-col flex-1 items-center p-6 mb-10">
            <div class="container flex flex-row flex-wrap mx-auto">
                <div class="w-full flex flex-wrap z-10">
                    <div class="card m-1 w-full">
                        <div class="card-content items-start justify-start p-4">
                            <form method="POST" class="w-full" action="{{ route('forums.comment.update', [$comment]) }}">
                                @csrf
                                @method('PATCH')

                                <div class="w-full my-2 p-3">
                                    <div class="flex items-center">
                                        <div class="flex-grow text-neutral-200 w-full my-4">
                                            <comment-editor-component content="{{ $comment->content }}"></comment-editor-component>
                                        </div>
                                    </div>
                                </div>

                                <div class="w-full p-3 text-left">
                                    <div class="flex flex-col items-end">
                                        <button type="submit" class="button button-primary button-hero mx-0 my-2">Update Comment</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
