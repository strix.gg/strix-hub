@extends('layouts.main')

@section('content')
    <main class="block">

        <section class="backdrop">

            <div class="backdrop-container">
                <img class="lazy backdrop-container__img" data-src="{{ $thread->board->category->getFirstMediaUrl('cover') }}" alt="">
            </div>

            @include('partials.spacer')

            <div class="container flex flex-row flex-wrap mx-auto">
                <div class="relative w-full flex flex-wrap z-10">
                    <div class="relative w-full rounded py-12 flex items-center">
                        <h4 class="text-neutral-100 font-display-1 text-3xl pr-3 md:p-0">
                            {{ $thread->title }}
                        </h4>

                        <div class="ml-auto">
                            <a class="button button-secondary p-4" href="#reply">
                                <i class="fad fa-reply fa-fw"></i>
                            </a>
                        </div>
                    </div>

                    <div class="ml-auto mb-4">
                        {{ $comments->links() }}
                    </div>

                    @if($comments->onFirstPage())
                        <article class="card m-1 w-full">
                            <div class="card-content post-container">

                                <div class="postbit-user">

                                    <div class="lazy postbit-user-container">
                                        <img class="lazy postbit-user__cover" data-src="{{ $thread->user->getMediaUrl('postbit_cover') }}" alt="">
                                        <div class="postbit-user-mask--small"></div>
                                        <div class="postbit-user-mask--large"></div>
                                    </div>

                                    <div class="postbit-user-info">
                                        <div class="postbit-user-info__details">
                                            <h4>
                                                <span style="color: {{ $thread->user->getColor() }} !important;">{{ $thread->user->name }}</span>
                                                @if($thread->user->isStaff())
                                                    <div class="inline-block" v-tippy content="User is a verified staff member.">
                                                        <i class="fad fa-badge-check fa-sm fa-fw" ></i>
                                                    </div>
                                                @endif
                                            </h4>
                                            <user-hovercard-component slug="{{ $thread->user->slug }}">
                                                <a href="{{ route('users.show', $thread->user) }}">
                                                    <img class="lazy postbit-user-info__details__img" data-src="{{ $thread->user->getMediaUrl('avatar') }}" alt="">
                                                </a>
                                            </user-hovercard-component>
                                        </div>

                                        <div class="postbit-user__avatar">
                                            <div class="inline-block @if($thread->user->isOnline()) text-green-500 @else  text-neutral-200 @endif">
                                                <i class="fad fa-circle fa-fw"></i>
                                                <span class="inline-block">@if($thread->user->isOnline()) online @else offline @endif</span>
                                            </div>
                                            <p class="postbit-user__tagline">{{ $thread->user->tagline }}</p>
                                            <div class="postbit-user__avatar-container">
                                                <img class="lazy" data-src="{{ $thread->user->preferredRole()->getMediaUrl('userbar') }}" alt="">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="thread-container">
                                    <header class="thread-header">
                                        <div class="thread-header__time">
                                            {{ $thread->created_at->diffForHumans() }}
                                        </div>
                                        <div class="thread-header__id">
                                            <a href="#{{$thread->id}}">
                                                <i class="fad fa-clipboard fa-fw mr-1"></i>
                                                #{{ \Str::limit($thread->id, 8, '') }}
                                            </a>
                                        </div>
                                        <div class="thread-header__actions">
                                            <button>
                                                <i class="fad fa-times-circle fa-fw text-red-500"></i>
                                            </button>
                                        </div>
                                    </header>
                                    <div class="thread-content">
                                        <static-editor-component content="{{ $thread->content }}"></static-editor-component>
                                    </div>
                                    <footer class="thread-footer">
                                        <div class="thread-footer__content">
                                            @can('edit thread')
                                                <div class="inline-block mx-1">
                                                    <button @click="$modal.show('edit-modal-{{$thread->id}}')" class="button button-secondary">
                                                        <i class="fad fa-pencil fa-fw mr-1"></i>
                                                        Edit
                                                    </button>
                                                </div>
                                            @elseif($thread->user->is(Auth::user()))
                                                <div class="inline-block mx-1">
                                                    <button @click="$modal.show('edit-modal-{{$thread->id}}')" class="button button-secondary">
                                                        <i class="fad fa-pencil fa-fw mr-1"></i>
                                                        Edit
                                                    </button>
                                                </div>
                                            @endcan

                                            <div class="hidden inline-block mx-1">
                                                <button class="button button-secondary">
                                                    <i class="fad fa-reply fa-fw mr-1"></i>
                                                    Reply
                                                </button>
                                            </div>
                                        </div>
                                    </footer>
                                </div>

                            </div>
                        </article>
                    @endif
                </div>
            </div>
        </section>

        @include('partials.modals.thread.edit.modal')

        <section class="relative flex flex-col flex-1 items-center p-6 pt-0 mb-10">
            <div class="container flex flex-row flex-wrap mx-auto">

                @foreach($comments as $comment)
                    <article id="{{ $comment->id }}" class="card m-1 w-full">
                        <div class="card-content post-container">

                            <div class="postbit-user">

                                <div class="lazy postbit-user-container">
                                    <img class="lazy postbit-user__cover" data-src="{{ $comment->commenter->getMediaUrl('postbit_cover') }}" alt="">
                                    <div class="postbit-user-mask--small"></div>
                                    <div class="postbit-user-mask--large"></div>
                                </div>

                                <div class="postbit-user-info">
                                    <div class="postbit-user-info__details">
                                        <h4>
                                            <span style="color: {{ $comment->commenter->getColor() }} !important;">{{ $comment->commenter->name }}</span>
                                            @if($comment->commenter->isStaff())
                                                <div class="inline-block" v-tippy content="User is a verified staff member.">
                                                    <i class="fad fa-badge-check fa-sm fa-fw" ></i>
                                                </div>
                                            @endif
                                        </h4>
                                        <user-hovercard-component slug="{{ $comment->commenter->slug }}">
                                            <a href="{{ route('users.show', $comment->commenter) }}">
                                                <img class="lazy postbit-user-info__details__img" data-src="{{ $comment->commenter->getMediaUrl('avatar') }}" alt="">
                                            </a>
                                        </user-hovercard-component>
                                    </div>

                                    <div class="postbit-user__avatar">
                                        <div class="inline-block @if($comment->commenter->isOnline()) text-green-500 @else  text-neutral-200 @endif">
                                            <i class="fad fa-circle fa-fw"></i>
                                            <span class="inline-block">@if($comment->commenter->isOnline()) online @else offline @endif</span>
                                        </div>
                                        <p class="postbit-user__tagline">{{ $comment->commenter->tagline }}</p>
                                        <div class="postbit-user__avatar-container">
                                            <img class="lazy" data-src="{{ $comment->commenter->preferredRole()->getMediaUrl('userbar') }}" alt="">
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="thread-container">
                                <header class="thread-header">
                                    <div class="thread-header__time">
                                        {{ $comment->created_at->diffForHumans() }}
                                    </div>
                                    <div class="thread-header__id">
                                        <a href="#{{$comment->id}}">
                                            <i class="fad fa-clipboard fa-fw mr-1"></i>
                                            #{{ \Str::limit($comment->id, 8, '') }}
                                        </a>
                                    </div>
                                    <div class="thread-header__actions">
                                        <button>
                                            <i class="fad fa-times-circle fa-fw text-red-500"></i>
                                        </button>
                                    </div>
                                </header>
                                <div class="thread-content">
                                    <static-editor-component content="{{ $comment->content }}"></static-editor-component>
                                </div>
                                <footer class="thread-footer">
                                    <div class="thread-footer__content">

                                        @can('edit comment')
                                            <div class="inline-block mx-1">
                                                <button @click="$modal.show('edit-modal-{{$comment->id}}')" class="button button-secondary">
                                                    <i class="fad fa-pencil fa-fw mr-1"></i>
                                                    Edit
                                                </button>
                                            </div>
                                        @elseif($comment->commenter->is(Auth::user()))
                                            <div class="inline-block mx-1">
                                                <button @click="$modal.show('edit-modal-{{$comment->id}}')" class="button button-secondary">
                                                    <i class="fad fa-pencil fa-fw mr-1"></i>
                                                    Edit
                                                </button>
                                            </div>
                                        @endcan

                                        <div class="hidden inline-block mx-1">
                                            <button class="button button-secondary">
                                                <i class="fad fa-reply fa-fw mr-1"></i>
                                                Reply
                                            </button>
                                        </div>
                                    </div>
                                </footer>
                            </div>

                        </div>
                    </article>
                    @include('partials.modals.comments.edit.modal')
                @endforeach

                <article id="reply" class="card m-1 w-full">
                    <div class="card-content post-container">

                        <div class="postbit-user">

                            <div class="lazy postbit-user-container">
                                <img class="lazy postbit-user__cover" data-src="{{ Auth::user()->getMediaUrl('postbit_cover') }}" alt="">
                                <div class="postbit-user-mask--small"></div>
                                <div class="postbit-user-mask--large"></div>
                            </div>

                            <div class="postbit-user-info">
                                <div class="postbit-user-info__details">
                                    <h4>
                                        <span style="color: {{ Auth::user()->getColor() }} !important;">{{ Auth::user()->name }}</span>
                                        @if(Auth::user()->isStaff())
                                            <div class="inline-block" v-tippy content="User is a verified staff member.">
                                                <i class="fad fa-badge-check fa-sm fa-fw" ></i>
                                            </div>
                                        @endif
                                    </h4>
                                    <a href="{{ route('users.show', Auth::user()) }}">
                                        <img class="lazy" data-src="{{ Auth::user()->getMediaUrl('avatar') }}" alt="">
                                    </a>
                                </div>

                                <div class="postbit-user__avatar">
                                    <div class="inline-block @if(Auth::user()->isOnline()) text-green-500 @else  text-neutral-200 @endif">
                                        <i class="fad fa-circle fa-fw"></i>
                                        <span class="inline-block">@if(Auth::user()->isOnline()) online @else offline @endif</span>
                                    </div>
                                    <p class="postbit-user__tagline">{{ Auth::user()->tagline }}</p>
                                    <div class="postbit-user__avatar-container">
                                        <img class="lazy" data-src="{{ Auth::user()->preferredRole()->getMediaUrl('userbar') }}" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="thread-container">
                            <div class="thread-content">
                                <form class="h-full" method="POST" action="{{ route('forums.thread.comment.store', [$thread]) }}">
                                    @csrf

                                    <div class="flex flex-col items-center h-full">
                                        <div class="flex-grow text-neutral-200 w-full my-4">
                                            <editor-component menubar_classes="border-t-0"></editor-component>
                                        </div>
                                        <button class="relative button button-primary button-hero ml-auto" type="submit">
                                            Create Comment
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </article>

                <div class="ml-auto mt-4">
                    {{ $comments->links() }}
                </div>
            </div>
        </section>
    </main>
@endsection
