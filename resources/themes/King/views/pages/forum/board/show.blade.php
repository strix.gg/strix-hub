@extends('layouts.main')

@section('content')
    <main class="block">
        <section class="backdrop">

            <div class="backdrop-container">
                <img class="lazy backdrop-container__img" data-src="{{ $board->category->getFirstMediaUrl('cover') }}" alt="">
            </div>

            @include('partials.spacer')

            <div class="container forum-container">
                <div class="w-full flex flex-wrap z-10">
                    <div class="py-12 px-32 text-center justify-center w-full">
                        <h3 class="text-neutral-100 font-display-1 font-bold text-xl">
                            {{ $board->title }}
                        </h3>
                    </div>

                    @can('create thread')
                        <a href="{{ route('forums.thread.create', $board) }}" class="button button-primary button-hero ml-auto">
                            Create Thread
                        </a>
                    @endcan

                    <div class="card m-1 w-full">
                        <div class="card-content items-start justify-start">
                            @foreach($threads as $thread)
                                <div class="card-grid-item forum-thread-body">
                                    <div class="forum-thread">
                                        <div class="forum-thread-avatar">
                                            <a href={{ route('users.show', $thread->user) }}#">
                                                <user-hovercard-component slug="{{ $thread->user->slug }}">
                                                    <img class="lazy" data-src="{{ $thread->user->getMediaUrl('avatar') ?? null }}" alt="">
                                                </user-hovercard-component>
                                            </a>
                                        </div>
                                        <div class="forum-thread-info">
                                            <h3 class="forum-thread-info__title">
                                                <a href="{{ route('forums.thread.show', [$thread, $thread->slug]) }}">
                                                    {{ $thread->title }}
                                                </a>
                                            </h3>
                                            <span>
                                                <a class="text-primary-500" href="{{ route('users.show', $thread->user) }}" style="color: {{ $thread->user->getColor() }} !important;">
                                                    {{ $thread->user->name }}
                                                </a>
                                        </span>
                                        </div>
                                        <div class="forum-thread-stats">
                                            <div class="text-neutral-300">
                                                <i class="fad fa-fw fa-comments"></i>
                                                <span class="text-white text-right">{{ $thread->comment_count }}</span>
                                            </div>
                                            <div class="text-neutral-300">
                                                <i class="fad fa-fw fa-clock"></i>
                                                <span class="text-white text-right">{{ $thread->created_at->diffForHumans() }}</span>
                                            </div>
                                        </div>
                                        @if($thread->comments()->exists())
                                            <div class="forum-thread-last-comment">
                                                <a class="text-primary-500" href="{{ route('forums.thread.show', [$thread, $thread->slug]) }}" style="color: {{ $thread->comments->last()->commenter->getColor() }} !important;">
                                                    {{ $thread->comments->last()->commenter->name }}
                                                </a>
                                                <div class="block">
                                                    <span>{{ $thread->comments->last()->created_at->diffForHumans() }}</span>
                                                </div>
                                            </div>

                                        @else
                                            <div class="forum-thread-last-comment">
                                                <div class="relative truncate z-20 text-neutral-100">
                                                    No Comments yet :(
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </main>
@endsection
