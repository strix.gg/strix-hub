@extends('layouts.main')


@section('content')
    <main class="block">

        <section class="backdrop">

            <div class="backdrop-container">
                <img class="lazy backdrop-container__img" data-src="{{ $user->getMediaUrl('cover') }}" alt="">
            </div>

            @include('partials.spacer')

            <div class="profile-header container">
                <div class="profile-header__content">

                    <div class="profile-picture">
                        <div class="profile-picture-container">
                            <div class="profile-picture-container__img">
                                <img class="lazy" data-src="{{ $user->getMediaUrl('avatar') }}" alt="profile picture">

                                <div class="profile-picture__overlay">
                                    @can('edit user')
                                        <button class="m-auto p-6" @click="$modal.show('settings-modal')">
                                            <i class="fad fa-camera fa-fw fa-lg"></i>
                                        </button>
                                    @elseif($user->is(Auth::user()))
                                        <button class="m-auto p-6" @click="$modal.show('settings-modal')">
                                            <i class="fad fa-camera fa-fw fa-lg"></i>
                                        </button>
                                    @endcan
                                </div>

                            </div>
                        </div>

                        <div class="profile-name">

                            <h1 class="profile-name__container">
                                <span style="color: {{ $user->getColor() }}">{{ $user->name }}</span>
                                @if($user->isStaff())
                                    <div class="inline-block" v-tippy content="User is a verified staff member.">
                                        <i class="fad fa-badge-check fa-fw" ></i>
                                    </div>
                                @endif
                            </h1>

                            <p class="profile-name__phrase">
                                {{ $user->tagline ?? $user->name . ' has no tagline :(' }}
                            </p>

                        </div>
                    </div>


                    @can('edit user')
                        <button class="button button-neutral mx-1 h-10" @click="$modal.show('settings-modal')">
                            <i class="fad fa-cogs fa-fw mr-2"></i>
                            Settings
                        </button>
                    @elseif(Auth::user()->is($user))
                        <button class="button button-neutral mx-1 h-10" @click="$modal.show('settings-modal')">
                            <i class="fad fa-cogs fa-fw mr-2"></i>
                            Settings
                        </button>
                    @endcan
                </div>
            </div>
        </section>

        <section class="mb-10">
            <div class="container flex flex-row flex-1 flex-wrap mx-auto">
                <div class="w-full md:w-1/4 flex flex-col">
                    <div class="flex flex-row w-full">

                        <div class="card w-1/2 m-1">
                            <div class="card-content px-12 py-2">
                                <h1 class="text-neutral-100 text-xl">
                                    <i class="fad fa-minus-circle fa-fw fa-xs"></i>
                                    <span>{{ $user->reputation ?? 0 }}</span>
                                </h1>
                                <p class="text-neutral-300 text-sm">REPUTATION</p>
                            </div>
                        </div>
                        <div class="card w-1/2 m-1">
                            <div class="card-content px-12 py-2">
                                <h1 class="text-green-100 text-xl">{{ $user->stars ?? 0 }}</h1>
                                <p class="text-neutral-300 text-sm">STARS</p>
                            </div>
                        </div>

                    </div>

                    <div class="card m-1">
                        <div class="card-header">
                            <div class="z-10 text-white">
                                <i class="fad fa-user fa-fw mr-1"></i>
                                <span class="font-display-1">User Information</span>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-grid-item">
                                <div class="text-neutral-300 truncate">
                                    Username Changes:
                                </div>
                                <span class="text-neutral-100 truncate text-right">
                                    {{ $user->username_changes ?? 0 }}
                                </span>
                            </div>
                            <div class="card-grid-item">
                                <div class="text-neutral-300 truncate">
                                    Last Visit:
                                </div>

                                @if($user->isOnline())
                                    <span class="text-green-500 truncate text-right">
                                    <i class="fad fa-circle fa-fw"></i>
                                    Online
                                </span>
                                @else
                                    <span class="text-neutral-100 truncate text-right">
                                    <i class="fad fa-circle fa-fw"></i>
                                    {{ $user->last_visited_at->diffForHumans() }}
                                </span>
                                @endif
                            </div>
                            <div class="card-grid-item">
                                <div class="text-neutral-300 truncate">
                                    Joined:
                                </div>
                                <span class="text-neutral-100 truncate text-right">
                                    {{ $user->created_at->diffForHumans() }}
                                </span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="w-full md:w-2/4 flex flex-1 flex-wrap">
                    <div class="card m-1 w-full">
                        <div class="card-header">
                            <div class="z-10 text-white">
                                <i class="fad fa-comments fa-fw mr-1"></i>
                                <span class="font-display-1">Comments</span>
                            </div>
                        </div>
                        <div class="card-content items-start justify-start">
                            <div class="card-grid-item justify-start items-start p-0" style="min-height: auto">
                                <div class="relative flex flex-col justify-start w-full z-10 p-3">
                                    <a class="relative flex flex-row items-center rounded-full mr-2" style="min-width: 2rem" href="{{ route('users.show', Auth::user()) }}">
                                        <img class="lazy h-8 w-8 rounded shadow-xl mr-2" data-src="{{ Auth::user()->getMediaUrl('avatar') }}" alt="">

                                        <span class="text-white text-base font-display-1 truncate" style="color: {{ Auth::user()->getColor() }}">{{ Auth::user()->name }}</span>
                                    </a>
                                    <div class="flex flex-col w-full">
                                        <span class="text-base break-all">
                                            <form method="POST" action="{{ route('users.comments.store', $user) }}">
                                                @csrf

                                                <div class="flex flex-col items-center">
                                                    <div class="flex-grow text-neutral-200 w-full my-4">
                                                        <editor-component menubar_classes="border-t-0"></editor-component>
                                                    </div>
                                                    <button class="relative button button-secondary ml-auto" type="submit">
                                                        Comment
                                                    </button>
                                                </div>
                                            </form>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            @foreach($comments as $comment)
                                <div class="card-grid-item justify-start items-start p-0 h-full">
                                    <div class="relative flex flex-col justify-start w-full z-10 p-3">
                                        <a class="relative flex flex-row items-center rounded-full mr-2" style="min-width: 2rem" href="{{ route('users.show', $comment->commenter) }}">
                                            <user-hovercard-component slug="{{ $comment->commenter->slug }}">
                                                <img class="lazy h-8 w-8 rounded shadow-xl mr-2" data-src="{{ $comment->commenter->getMediaUrl('avatar') }}" alt="">
                                            </user-hovercard-component>

                                            <span class="text-white text-base font-display-1 truncate" style="color: {{ $comment->commenter->getColor() }}">{{ $comment->commenter->name }}</span>

                                            <span class="text-neutral-300 text-right ml-auto">{{ $comment->created_at->diffForHumans() }}</span>
                                        </a>
                                        <div class="flex flex-col w-full">
                                            <span class="text-neutral-200 text-base break-all my-4">
                                                <static-editor-component content="{{ $comment->content }}"></static-editor-component>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="w-full md:w-1/4 flex">
                    <div class="flex flex-col w-full">

                        <div class="card m-1">
                            <div class="card-header">
                                <div class="z-10 text-white">
                                    <i class="fad fa-users fa-fw mr-1"></i>
                                    <span class="font-display-1">User Groups</span>
                                </div>
                            </div>

                            <div class="card-content items-start justify-start overflow-y-scroll">
                                @foreach($user->roles as $role)
                                    <div class="block w-full">
                                        <img class="lazy block mx-auto w-auto p-4" data-src="{{ $role->getMediaUrl('userbar') }}" alt="{{ $role->name }}">
                                    </div>
                                @endforeach
                            </div>

                        </div>

                        <div class="card m-1">
                            <div class="card-header">
                                <div class="z-10 text-white">
                                    <i class="fad fa-clipboard fa-fw mr-1"></i>
                                    <span class="font-display-1">Recent Threads</span>
                                </div>
                            </div>

                            <div class="card-content">
                                @foreach($threads as $thread)
                                    <div class="card-grid-item">
                                        <a class="text-neutral-100 truncate max-w-xxs mr-2" href="{{ route('forums.thread.show', [$thread, $thread->slug]) }}">
                                            {{ $thread->title }}
                                        </a>
                                        <span class="text-neutral-300 truncate text-right">
                                    {{ $thread->created_at->diffForHumans() }}
                                </span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @include('partials.modals.user.settings.modal')
    </main>
@endsection
