<div class="m-auto flex flex-col items-center justify-center">
    <img class="lazy w-12 h-12" data-src="{{ asset('static/king/currency/crown_tilted.svg') }}" alt="">
    <div class="text-neutral-100 font-bold text-lg text-center flex flex-col">
        Ready to Upgrade?
        <span class="text-sm font-normal">
                                        Unlock customizations with VIP and up!
                                    </span>
    </div>
    <div class="relative mt-12 text-white px-8 py-12 shadow neutral-gradient w-full max-w-3xl border-t-2"
         style="border-image: linear-gradient(to right, #47bfff 0%, #1a44c2 60%) 25%;">

        <ul class="flex mb-4 w-full">
            <li class="flex flex-col flex-wrap rounded p-6 w-full md:w-1/2 items-center justify-center text-center m-2" style="background: rgba(255, 255, 255, 0.06)">
                <h3 class="font-bold">King</h3>
                <img class="lazy" data-src="{{ \App\Models\Role::whereName('king')->first()->getMediaUrl('userbar') }}" alt="">
                <ul>
                    <li class="flex font-bold items-center text-left">
                        <i class="fad fa-check fa-fw mr-2"></i>
                        Forum Customizations
                    </li>
                    <li class="flex font-bold items-center text-left">
                        <i class="fad fa-check fa-fw mr-2"></i>
                        Cross-platform rank
                    </li>
                </ul>
            </li>
            <li class="flex flex-col flex-wrap rounded p-6 w-full md:w-1/2 items-center justify-center text-center m-2" style="background: rgba(255, 255, 255, 0.06)">
                <h3 class="font-bold">Knight</h3>
                <img class="lazy" data-src="{{ \App\Models\Role::whereName('knight')->first()->getMediaUrl('userbar') }}" alt="">
                <ul>
                    <li class="flex font-bold items-center text-left">
                        <i class="fad fa-check fa-fw mr-2"></i>
                        Forum Customizations
                    </li>
                    <li class="flex font-bold items-center text-left">
                        <i class="fad fa-check fa-fw mr-2"></i>
                        Cross-platform rank
                    </li>
                </ul>
            </li>
        </ul>

        <button class="button button-secondary mx-auto">Learn more about donating</button>
    </div>

</div>
