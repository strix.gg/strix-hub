<div class="relative banner bg-cover bg-center mt-4 my-12">
    <img class="lazy absolute inset-0 object-cover object-center h-full w-full" data-src="{{ asset('static/king/banners/store/kg_banner_background.png') }}" alt="">
    <div class="banner-section-left">
        <div class="banner-img">
            <img class="lazy" data-src="{{ asset('static/king/banners/store/kg_banner_left.png') }}" alt="">
        </div>
    </div>

    <div class="banner-section-middle">
        <div class="banner-body text-neutral-050">
            <h1 class="banner-header">
                Support us with purchasing crowns
            </h1>

            <p class="banner-sub">
                Use crowns in game on premium content like cosmetics, cash, and more!
            </p>

            <div class="flex">
                @auth
                    <button @click="$modal.show('store-modal')" class="mr-auto button button-transparent h-10">
                        <img class="lazy w-6 h-6 mr-2" data-src="{{ asset('static/king/currency/crown_tilted.svg') }}" alt="">
                        <span>Get Crowns</span>
                    </button>
                @endauth

                @guest
                    <a href="{{ route('login') }}" class="mr-auto button button-transparent h-10">
                        <span>Login</span>
                    </a>
                @endguest
            </div>

        </div>
    </div>

    <div class="banner-section-right">
        <div>
            <button></button>
        </div>
    </div>
</div>
