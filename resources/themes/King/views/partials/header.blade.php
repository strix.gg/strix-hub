<div class="navbar">

    <div class="navbar-group navbar-group-brand">
        <a href="#" class="navbar-group-brand__img">
            <img class="lazy" data-src="{{ asset('static/king/logo_full.svg') }}" alt=""/>
        </a>
    </div>

    <ul class="navbar-group">

        <li class="nav-item">
            <a class="nav-link" href="{{ route('index') }}">
                    <span>
                        Home
                    </span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ route('forums.index') }}">
                    <span>
                        Forums
                    </span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#" content="Coming Soon" v-tippy>
                    <span>
                        Community
                    </span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#" content="Coming Soon" v-tippy>
                    <span>
                        Social
                    </span>
            </a>
        </li>

    </ul>

    <ul class="navbar-group navbar-group--float-right">
        @auth
            <li class="nav-item">
                <a class="nav-link button button-secondary mr-4 h-10" @click="$modal.show('store-modal')">
                    <img class="lazy w-6 h-6 mr-2" data-src="{{ asset('static/king/currency/crown_tilted.svg') }}" alt="">
                    <span>
                        Get Crowns
                    </span>
                </a>
            </li>
        @endauth
        <li class="nav-item">
            @auth
                <a href="{{ route('users.show', Auth::user()->slug) }}">
                    <img class="lazy w-12 h-12 rounded" data-src="{{ Auth::user()->getMediaUrl('avatar') }}" alt="">
                </a>
            @endauth

            @guest
                <a href="{{ route('login') }}" class="nav-link">
                        <span>
                            Login
                        </span>
                </a>
            @endguest
        </li>
    </ul>
</div>
