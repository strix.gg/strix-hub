@can('edit thread')
    <modal-component v-cloak name="edit-modal-{{$thread->id}}" classes="card w-full fixed inset-0 overflow-y-auto">
        <div class="container mx-auto">
            @include('partials.errors')

            <form method="POST" class="w-full mt-10" action="{{ route('forums.thread.update', $thread) }}">
                @csrf
                @method('PATCH')

                <div class="bg-neutral-500 rounded text-neutral-200">
                    <label class="form-input-container">
                        <input id="title" type="text" name="title"
                               class="form-input rounded-b-none border-0 p-4"
                               value="{{ old('title') ?? $thread->title }}"
                               placeholder="Title"
                               required autocomplete="title">
                    </label>

                    @error('name')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <editor-component content="{{ $thread->content }}" editor_classes="rounded-t-none" editor_classes=""></editor-component>
                </div>

                <div class="w-full p-3 text-left">
                    <div class="flex flex-col items-end">
                        <button type="submit" class="button button-primary button-hero mx-0 my-2">Update Thread</button>
                    </div>
                </div>
            </form>
        </div>
    </modal-component>
@elseif($thread->user->is(Auth::user()))
    <modal-component name="edit-modal-{{$thread->id}}" classes="card w-full fixed inset-0 overflow-y-auto">
        <div class="container mx-auto">
            <form method="POST" class="w-full mt-10" action="{{ route('forums.thread.update', [$thread]) }}">
                @csrf
                @method('PATCH')
                <div class="w-full my-2 p-3">
                    <label class="form-input-container">
                        <input id="title" type="text" name="title"
                               class="form-input form-input-primary @error('title') border-red-500 @enderror"
                               value="{{ old('title') ?? $thread->title }}"
                               placeholder=" "
                               required autocomplete="title">
                        <div class="form-input-label">title</div>
                    </label>

                    @error('name')
                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

                <div class="w-full my-2 p-3">
                    <div class="flex items-center">
                        <div class="flex-grow text-neutral-200 w-full my-4">
                            <editor-component content="{{ $thread->content }}" menubar_classes="border-t-0"></editor-component>
                        </div>
                    </div>
                </div>

                <div class="w-full p-3 text-left">
                    <div class="flex justify-end items-center my-2">
                        <a class="button button-secondary mx-1" @click="$modal.show('edit-modal-{{$thread->id}}')">Cancel</a>
                        <button type="submit" class="button button-primary button-hero mx-1">Update Thread</button>
                    </div>
                </div>
            </form>
        </div>
    </modal-component>
@endcan
