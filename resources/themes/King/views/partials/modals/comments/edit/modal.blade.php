@can('edit comment')
    <modal-component v-cloak name="edit-modal-{{$comment->id}}" classes="card w-full fixed inset-0 overflow-y-auto">
        <div class="container mx-auto">
            <form method="POST" class="w-full mt-10" action="{{ route('forums.comment.update', [$comment]) }}">
                @csrf
                @method('PATCH')

                <div class="w-full my-2 p-3">
                    <div class="flex items-center">
                        <div class="flex-grow text-neutral-200 w-full my-4">
                            <editor-component content="{{ $comment->content }}" menubar_classes="border-t-0"></editor-component>
                        </div>
                    </div>
                </div>

                <div class="w-full p-3 text-left">
                    <div class="flex justify-end items-center my-2">
                        <a class="button button-secondary mx-1" @click="$modal.hide('edit-modal-{{$comment->id}}')">Cancel</a>
                        <button type="submit" class="button button-primary button-hero mx-1">Update Thread</button>
                    </div>
                </div>
            </form>
        </div>
    </modal-component>
@elseif($comment->commenter->is(Auth::user()))
    <modal-component v-cloak name="edit-modal-{{$comment->id}}" classes="card w-full fixed inset-0 overflow-y-auto">
        <div class="container mx-auto">
            <form method="POST" class="w-full mt-10" action="{{ route('forums.comment.update', [$comment]) }}">
                @csrf
                @method('PATCH')

                <div class="w-full my-2 p-3">
                    <div class="flex items-center">
                        <div class="flex-grow text-neutral-200 w-full my-4">
                            <editor-component content="{{ $comment->content }}" menubar_classes="border-t-0"></editor-component>
                        </div>
                    </div>
                </div>

                <div class="w-full p-3 text-left">
                    <div class="flex justify-end items-center my-2">
                        <a class="button button-secondary mx-1" @click="$modal.hide('edit-modal-{{$comment->id}}')">Cancel</a>
                        <button type="submit" class="button button-primary button-hero mx-1">Update Thread</button>
                    </div>
                </div>
            </form>
        </div>
    </modal-component>
@endcan
