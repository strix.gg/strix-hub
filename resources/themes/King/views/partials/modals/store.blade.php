@auth
    <modal-component v-cloak name="store-modal" classes="crown-modal bg-transparent bg-neutral-600 max-w-4xl w-full overflow-y-hidden">
        <div>
            <div class="text-center">
                <h1 class="text-neutral-050 pt-16 text-2xl">Purchase Crowns</h1>
                <p class="text-neutral-300">
                    Use crowns in game on premium content like cosmetics, cash, and more!
                </p>
            </div>
            <div class="relative flex min-h-0">

                <div class="crown-container">
                    @foreach(\App\Models\Store\Product::orderBy('price', 'asc')->get() as $credit)
                        <form method="POST" action="{{ route('payment.gateway.paypal.express', $credit) }}">
                            <button class="crown-tile">
                                <h1 class="crown-title">{{ $credit->title }}</h1>
                                <p class="crown-amount">{{ $credit->base_amount + $credit->bonus_amount }}</p>
                                <div class="crown-image">
                                    <img class="lazy" data-src="https://xforgeassets001.xboxlive.com/xuid-2535473787585366-public/2c0cc92b-5946-4b68-a6dc-d6f582b96ef5/Pile-of-Embers.png?height=90" alt="">
                                </div>
                                <p class="crown-cost">
                                    ${{ $credit->price / 100 }}
                                </p>
                            </button>
                        </form>
                    @endforeach
                </div>

                <div class="crown-shadow crown-shadow--top"></div>
                <div class="crown-shadow crown-shadow--bottom"></div>
            </div>

            <div class="flex items-center justify-center h-20">
                <div class="text-neutral-300 flex items-center">
                    Your crown balance:
                    <img class="lazy ml-2 mr-1 h-6 w-6" data-src="{{ asset('static/king/currency/crown_tilted.svg') }}" alt="">
                    <span class="text-white">{{ Auth::user()->credit->balance }}</span>
                </div>
            </div>

        </div>
    </modal-component>
@endauth
