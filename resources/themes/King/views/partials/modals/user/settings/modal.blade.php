@auth
    @can('edit user')
        <modal-component v-cloak name="settings-modal" classes="w-full fixed inset-0 card ">
            <profile-tabs-component>
                <profile-tab-component title="Overview">
                    <div class="p-4">
                        <form method="POST" action="{{ route('users.update', $user) }}">
                            @csrf
                            @method('PATCH')

                            <div class="w-full md:w-1/3 min-h-screen">
                                <div class="flex flex-wrap">
                                    <div class="w-full my-2">
                                        <label class="form-input-container">
                                            <input id="name" type="text" name="name"
                                                   class="form-input form-input-primary @error('name') border-red-500 @enderror"
                                                   value="{{ old('name') ?? $user->name }}"
                                                   placeholder=" "
                                                   required autocomplete="name">
                                            <div class="form-input-label">Name</div>
                                        </label>

                                        @error('name')
                                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="w-full my-2 p-3">
                                        <label class="form-input-container">
                                            <input id="password" type="password" name="password"
                                                   class="form-input form-input-primary @error('email') border-red-500 @enderror"
                                                   value="{{ old('password') }}"
                                                   placeholder=" "
                                                   autocomplete="new-password">
                                            <div class="form-input-label">Password</div>
                                        </label>

                                        @error('password')
                                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                        @enderror
                                    </div>

                                    <div class="w-full my-2 p-3">
                                        <label class="form-input-container">
                                            <input id="password-confirm" type="password" name="password_confirmation"
                                                   class="form-input form-input-primary @error('email') border-red-500 @enderror"
                                                   value="{{ old('password') }}"
                                                   placeholder=" "
                                                   autocomplete="new-password">
                                            <div class="form-input-label">Password Confirmation</div>
                                        </label>
                                    </div>

                                    <div class="w-full my-2 p-3">
                                        <label class="form-input-container">
                                            <input id="tagline" type="text" name="tagline"
                                                   class="form-input form-input-primary @error('tagline') border-red-500 @enderror"
                                                   value="{{ old('tagline') ?? $user->tagline }}"
                                                   placeholder=" "
                                                   required autocomplete="tagline">
                                            <div class="form-input-label">Tagline</div>
                                        </label>

                                        @error('tagline')
                                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="w-full p-3 text-left">
                                        <div class="flex flex-col">
                                            <button type="submit" class="button button-primary button-hero mx-0 my-2 w-full md:w-auto">Update Profile</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </profile-tab-component>

                <profile-tab-component title="Media">
                    <div class="p-4">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('users.media.update', $user) }}">
                            @csrf
                            @method('PATCH')

                            <div class="w-full min-h-screen">
                                <div class="flex flex-wrap">

                                    <div class="w-full md:p-3">
                                        <div class="flex items-center">
                                            <div class="w-1/4 md:w-auto">
                                                <div class="h-24 w-24 rounded px-4 py-6 bg-neutral-500 border border-neutral-400 @error('avatar') border-red-500 @enderror text-neutral-500" style="background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url({{ $user->getFirstMediaUrl('avatar') }})"></div>
                                            </div>

                                            <div class="w-3/4 md:w-auto flex flex-col ml-5">
                                                <span class="text-neutral-100 mt-2 text-base leading-normal">Upload Avatar</span>
                                                <span class="text-neutral-200">We recommend a square image at least 256x256px.</span>

                                                <label class="button button-secondary button-hero h-auto text-neutral-100 mx-0 m-2">
                                                    <input name="avatar" type='file' class="hidden" />
                                                    <span>Upload Avatar</span>
                                                </label>
                                            </div>
                                        </div>

                                        @error('avatar')
                                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    @if($user->isPremium())
                                        <div class="w-full md:p-3 md:w-1/2 mt-3">
                                            <div class="flex items-center md:block">
                                                <div class="w-1/4 md:w-auto">
                                                    <div class="h-24 w-24 md:h-48 md:w-160 px-4 py-6 rounded bg-neutral-500 rounded border border-neutral-400 @error('avatar') border-red-500 @enderror text-neutral-500" style="background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url({{ $user->getFirstMediaUrl('cover') }})"></div>
                                                </div>

                                                <div class="ml-5 md:m-0 w-3/4 flex flex-col">
                                                    <span class="text-neutral-100 mt-2 text-base leading-normal">Upload Cover</span>
                                                    <span class="text-neutral-200">We recommend a wide image, at least 1420x420px. This banner will be displayed on the background of your user profile.</span>

                                                    <label class="button button-secondary button-hero text-neutral-100 mx-0 m-2">
                                                        <input name="cover" type='file' class="hidden" />
                                                        <span>Upload Cover</span>
                                                    </label>
                                                </div>
                                            </div>

                                            @error('cover')
                                            <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="w-full md:p-3 md:w-1/2 mt-3">
                                            <div class="flex md:block">
                                                <div class="w-1/4 ">

                                                    <div class="h-24 w-24 md:h-48 md:w-64 mx-auto md:m-0 rounded px-4 py-6 bg-neutral-500 rounded border border-neutral-400 @error('postbit_cover') border-red-500 @enderror text-neutral-500" style="background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url({{ $user->getFirstMediaUrl('postbit_cover') }})">

                                                    </div>
                                                </div>

                                                <div class="w-3/4 ml-5 md:m-0 flex flex-col">
                                                    <span class="text-neutral-100 mt-2 text-base leading-normal">Upload Postbit Cover</span>
                                                    <span class="text-neutral-200">We recommend a boxed image, at least 200x280px. This postbit cover will be displayed on the background of your threads / comments.</span>

                                                    <label class="button button-secondary button-hero text-neutral-100 mx-0 m-2">
                                                        <input name="postbit_cover" type='file' class="hidden" />
                                                        <span>Upload Postbit Cover</span>
                                                    </label>
                                                </div>
                                            </div>

                                            @error('postbit_cover')
                                            <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    @else
                                        @include('partials.store.upgrade')
                                    @endif

                                    <div class="w-full p-3 text-left">
                                        <div class="flex flex-col mt-3 max-w-xl">
                                            <button type="submit" class="button button-primary button-hero mx-0 my-2 w-full md:w-auto">Update Media</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </profile-tab-component>

                <profile-tab-component title="Premium">
                    @if($user->isPremium())
                        <div class="p-4 w-full flex flex-col flex-wrap md:flex-row">
                            <form method="POST" class="w-1/3" enctype="multipart/form-data" action="{{ route('users.role.update', $user) }}">
                                @csrf
                                @method('PATCH')

                                <div class="w-full min-h-screen">
                                    <div class="flex flex-wrap">

                                        <div class="w-full my-2 p-3">
                                            <label class="form-input-container">
                                                <span class="text-gray-700">Select Preferred Role</span>
                                                <select name="role" class="form-select block w-full mt-1">
                                                    @foreach($user->roles as $role)
                                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                    @endforeach
                                                </select>
                                            </label>

                                            @error('role')
                                            <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="w-full p-3 text-left">
                                            <div class="flex flex-col mt-3">
                                                <button type="submit" class="button button-primary button-hero mx-0 my-2 w-full md:w-auto">Update Role</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            @if($user->color()->exists())
                                <form method="POST" class="w-full md:w-1/3" enctype="multipart/form-data" action="{{ route('users.color.update', [$user, $user->color]) }}">
                                    @csrf
                                    @method('PATCH')

                                    <div class="w-full min-h-screen">
                                        <div class="flex flex-wrap">

                                            <div class="w-full my-2 p-3">
                                                <color-component show_details="true"></color-component>

                                                @error('color')
                                                <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>

                                            <div class="w-full p-3 text-left">
                                                <div class="flex flex-col mt-3">
                                                    <button type="submit" class="button button-primary button-hero mx-0 my-2 w-full md:w-auto">Update Color</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @else
                                <form method="POST" class="w-full md:w-1/3" enctype="multipart/form-data" action="{{ route('users.color.store', $user) }}">
                                    @csrf

                                    <div class="w-full min-h-screen">
                                        <div class="flex flex-wrap">

                                            <div class="w-full my-2 p-3">
                                                <color-component show_details="true"></color-component>

                                                @error('color')
                                                <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>

                                            <div class="w-full p-3 text-left">
                                                <div class="flex flex-col mt-3">
                                                    <button type="submit" class="button button-primary button-hero mx-0 my-2 w-full md:w-auto">Update Color</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        </div>
                    @else
                        <div class="p-4 w-full min-h-screen pt-20">
                            @include('partials.store.upgrade')
                        </div>

                    @endif
                </profile-tab-component>

            </profile-tabs-component>
        </modal-component>
    @elseif(Auth::user()->is($user))
        <modal-component v-cloak name="settings-modal" classes="w-full fixed inset-0 card ">
            <profile-tabs-component>
                <profile-tab-component title="Overview">
                    <div class="p-4">
                        <form method="POST" action="{{ route('users.update', $user) }}">
                            @csrf
                            @method('PATCH')

                            <div class="w-full md:w-1/3 min-h-screen">
                                <div class="flex flex-wrap">
                                    <div class="w-full my-2">
                                        <label class="form-input-container">
                                            <input id="name" type="text" name="name"
                                                   class="form-input form-input-primary @error('name') border-red-500 @enderror"
                                                   value="{{ old('name') ?? $user->name }}"
                                                   placeholder=" "
                                                   required autocomplete="name">
                                            <div class="form-input-label">Name</div>
                                        </label>

                                        @error('name')
                                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="w-full my-2 p-3">
                                        <label class="form-input-container">
                                            <input id="password" type="password" name="password"
                                                   class="form-input form-input-primary @error('email') border-red-500 @enderror"
                                                   value="{{ old('password') }}"
                                                   placeholder=" "
                                                   autocomplete="new-password">
                                            <div class="form-input-label">Password</div>
                                        </label>

                                        @error('password')
                                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                        @enderror
                                    </div>

                                    <div class="w-full my-2 p-3">
                                        <label class="form-input-container">
                                            <input id="password-confirm" type="password" name="password_confirmation"
                                                   class="form-input form-input-primary @error('email') border-red-500 @enderror"
                                                   value="{{ old('password') }}"
                                                   placeholder=" "
                                                   autocomplete="new-password">
                                            <div class="form-input-label">Password Confirmation</div>
                                        </label>
                                    </div>

                                    <div class="w-full my-2 p-3">
                                        <label class="form-input-container">
                                            <input id="tagline" type="text" name="tagline"
                                                   class="form-input form-input-primary @error('tagline') border-red-500 @enderror"
                                                   value="{{ old('tagline') ?? $user->tagline }}"
                                                   placeholder=" "
                                                   required autocomplete="tagline">
                                            <div class="form-input-label">Tagline</div>
                                        </label>

                                        @error('tagline')
                                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="w-full p-3 text-left">
                                        <div class="flex flex-col">
                                            <button type="submit" class="button button-primary button-hero mx-0 my-2 w-full md:w-auto">Update Profile</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </profile-tab-component>

                <profile-tab-component title="Media">
                    <div class="p-4">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('users.media.update', $user) }}">
                            @csrf
                            @method('PATCH')

                            <div class="w-full min-h-screen">
                                <div class="flex flex-wrap">

                                    <div class="w-full md:p-3">
                                        <div class="flex items-center">
                                            <div class="w-1/4 md:w-auto">
                                                <div class="h-24 w-24 rounded px-4 py-6 bg-neutral-500 border border-neutral-400 @error('avatar') border-red-500 @enderror text-neutral-500" style="background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url({{ $user->getFirstMediaUrl('avatar') }})"></div>
                                            </div>

                                            <div class="w-3/4 md:w-auto flex flex-col ml-5">
                                                <span class="text-neutral-100 mt-2 text-base leading-normal">Upload Avatar</span>
                                                <span class="text-neutral-200">We recommend a square image at least 256x256px.</span>

                                                <label class="button button-secondary button-hero h-auto text-neutral-100 mx-0 m-2">
                                                    <input name="avatar" type='file' class="hidden" />
                                                    <span>Upload Avatar</span>
                                                </label>
                                            </div>
                                        </div>

                                        @error('avatar')
                                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    @if($user->isPremium())
                                        <div class="w-full md:p-3 md:w-1/2 mt-3">
                                            <div class="flex items-center md:block">
                                                <div class="w-1/4 md:w-auto">
                                                    <div class="h-24 w-24 md:h-48 md:w-160 px-4 py-6 rounded bg-neutral-500 rounded border border-neutral-400 @error('avatar') border-red-500 @enderror text-neutral-500" style="background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url({{ $user->getFirstMediaUrl('cover') }})"></div>
                                                </div>

                                                <div class="ml-5 md:m-0 w-3/4 flex flex-col">
                                                    <span class="text-neutral-100 mt-2 text-base leading-normal">Upload Cover</span>
                                                    <span class="text-neutral-200">We recommend a wide image, at least 1420x420px. This banner will be displayed on the background of your user profile.</span>

                                                    <label class="button button-secondary button-hero text-neutral-100 mx-0 m-2">
                                                        <input name="cover" type='file' class="hidden" />
                                                        <span>Upload Cover</span>
                                                    </label>
                                                </div>
                                            </div>

                                            @error('cover')
                                            <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="w-full md:p-3 md:w-1/2 mt-3">
                                            <div class="flex md:block">
                                                <div class="w-1/4 ">

                                                    <div class="h-24 w-24 md:h-48 md:w-64 mx-auto md:m-0 rounded px-4 py-6 bg-neutral-500 rounded border border-neutral-400 @error('postbit_cover') border-red-500 @enderror text-neutral-500" style="background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url({{ $user->getFirstMediaUrl('postbit_cover') }})">

                                                    </div>
                                                </div>

                                                <div class="w-3/4 ml-5 md:m-0 flex flex-col">
                                                    <span class="text-neutral-100 mt-2 text-base leading-normal">Upload Postbit Cover</span>
                                                    <span class="text-neutral-200">We recommend a boxed image, at least 200x280px. This postbit cover will be displayed on the background of your threads / comments.</span>

                                                    <label class="button button-secondary button-hero text-neutral-100 mx-0 m-2">
                                                        <input name="postbit_cover" type='file' class="hidden" />
                                                        <span>Upload Postbit Cover</span>
                                                    </label>
                                                </div>
                                            </div>

                                            @error('postbit_cover')
                                            <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    @else
                                        @include('partials.store.upgrade')
                                    @endif

                                    <div class="w-full p-3 text-left">
                                        <div class="flex flex-col mt-3 max-w-xl">
                                            <button type="submit" class="button button-primary button-hero mx-0 my-2 w-full md:w-auto">Update Media</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </profile-tab-component>

                <profile-tab-component title="Premium">
                    @if($user->isPremium())
                        <div class="p-4 w-full flex flex-col flex-wrap md:flex-row">
                            <form method="POST" class="w-1/3" enctype="multipart/form-data" action="{{ route('users.role.update', $user) }}">
                                @csrf
                                @method('PATCH')

                                <div class="w-full min-h-screen">
                                    <div class="flex flex-wrap">

                                        <div class="w-full my-2 p-3">
                                            <label class="form-input-container">
                                                <span class="text-gray-700">Select Preferred Role</span>
                                                <select name="role" class="form-select block w-full mt-1">
                                                    @foreach($user->roles as $role)
                                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                    @endforeach
                                                </select>
                                            </label>

                                            @error('role')
                                            <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="w-full p-3 text-left">
                                            <div class="flex flex-col mt-3">
                                                <button type="submit" class="button button-primary button-hero mx-0 my-2 w-full md:w-auto">Update Role</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            @if($user->color()->exists())
                                <form method="POST" class="w-full md:w-1/3" enctype="multipart/form-data" action="{{ route('users.color.update', [$user, $user->color]) }}">
                                    @csrf
                                    @method('PATCH')

                                    <div class="w-full min-h-screen">
                                        <div class="flex flex-wrap">

                                            <div class="w-full my-2 p-3">
                                                <color-component show_details="true"></color-component>

                                                @error('color')
                                                <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>

                                            <div class="w-full p-3 text-left">
                                                <div class="flex flex-col mt-3">
                                                    <button type="submit" class="button button-primary button-hero mx-0 my-2 w-full md:w-auto">Update Color</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @else
                                <form method="POST" class="w-full md:w-1/3" enctype="multipart/form-data" action="{{ route('users.color.store', $user) }}">
                                    @csrf

                                    <div class="w-full min-h-screen">
                                        <div class="flex flex-wrap">

                                            <div class="w-full my-2 p-3">
                                                <color-component show_details="true"></color-component>

                                                @error('color')
                                                <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>

                                            <div class="w-full p-3 text-left">
                                                <div class="flex flex-col mt-3">
                                                    <button type="submit" class="button button-primary button-hero mx-0 my-2 w-full md:w-auto">Update Color</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        </div>
                    @else
                        <div class="p-4 w-full min-h-screen pt-20">
                            @include('partials.store.upgrade')
                        </div>

                    @endif
                </profile-tab-component>

            </profile-tabs-component>
        </modal-component>
    @endif

@endauth
