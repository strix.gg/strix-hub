module.exports = {
    important: true,
    theme: {
        fontFamily: {
            sans: [
                'Inter',
                '-apple-system',
                'BlinkMacSystemFont',
                '"Segoe UI"',
                'Roboto',
                '"Helvetica Neue"',
                'Arial',
                '"Noto Sans"',
                'sans-serif',
                '"Apple Color Emoji"',
                '"Segoe UI Emoji"',
                '"Segoe UI Symbol"',
                '"Noto Color Emoji"',
            ],
            serif: [
                'Georgia',
                'Cambria',
                '"Times New Roman"',
                'Times',
                'serif',
            ],
            mono: [
                'Menlo',
                'Monaco',
                'Consolas',
                '"Liberation Mono"',
                '"Courier New"',
                'monospace',
            ],
            'display-1': [
                'Inter',
                'sans-serif',
            ],
            'display-2': [
                'Inter',
                'sans-serif',
            ],
            'display-3': [
                'Inter',
                'sans-serif',
            ]
        },
        extend: {
            width: theme => ({
                80: '20rem',
                160: '40rem',
            }),
            colors: {
                primary: {
                    default: '#2E8FFF',
                    '050': '#F5FAFF',
                    '100': '#FAFCFF',
                    '200': '#C7E1FF',
                    '300': '#94C6FF',
                    '400': '#61ABFF',
                    '500': '#2E8FFF',
                    '600': '#0075FA',
                    '700': '#005DC7',
                    '800': '#004594',
                    '900': '#002D61'
                },
                neutral: {
                    default: '#393947',
                    '050': '#F7F7F8',
                    '100': '#E0E0E6',
                    '200': '#B3B3C2',
                    '300': '#85859D',
                    '400': '#5D5D74',
                    '500': '#393947',
                    '600': '#292933',
                    '700': '#19191F',
                    '800': '#09090B',
                    '900': '#020203'
                }
            }
        }
    },
    variants: {},
    plugins: [
        require('@tailwindcss/custom-forms')
    ]
}
