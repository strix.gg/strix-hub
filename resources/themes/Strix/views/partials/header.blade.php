<header class="relative hidden md:block text-base text-neutral-100 h-full w-full">

    <div class="relative flex items-center flex-wrap py-4 w-full max-w-6xl mx-auto h-full justify-center">
        <div class="logo mr-6 flex">
            <a href="#" class="h-12 w-12">
                <img src="{{ asset('static/strix_logo_light.svg') }}" alt=""/>
            </a>
        </div>

        <ul class="header-nav flex items-center mx-auto">
            <li class="inline-flex cursor-pointer opacity-75 hover:opacity-100 transition-all">
                <a href="{{ route('index') }}" class="py-3 px-6">
                    <span>Home</span>
                </a>
            </li>
            <li class="inline-flex cursor-pointer opacity-75 hover:opacity-100 transition-all">
                <a href="{{ route('forums.index') }}" class="py-3 px-6">
                    <span>Forums</span>
                </a>
            </li>
            <li class="inline-flex cursor-pointer button button-primary button-hero text-neutral-900" style="height: 38px; min-width: 120px;">
                <a href="https://store.strix.gg/" class="py-3 px-6">
                    <span>Store</span>
                </a>
            </li>
            <li class="inline-flex cursor-pointer opacity-75 hover:opacity-100 transition-all">
                <a href="/" class="py-3 px-6">
                    <span>Vote</span>
                </a>
            </li>
            <li class="inline-flex cursor-pointer opacity-75 hover:opacity-100 transition-all">
                <a href="/" class="py-3 px-6">
                    <span>Players</span>
                </a>
            </li>
        </ul>

        <ul class="header-nav flex items-center">
            <li class="inline-flex cursor-pointer button button-secondary text-neutral-100">
                @auth
                    <a href="{{ route('users.show', Auth::user()->slug) }}" class="px-4 py-1">
                        Profile
                    </a>
                @endauth

                @guest
                    <a href="{{ route('login') }}" class="px-4 py-1">
                        Login
                    </a>
                @endguest
            </li>
        </ul>
    </div>

    <hr class="relative border-neutral-900 opacity-25">

    <div class="relative flex items-center flex-wrap py-4 w-full h-full justify-center">
        <ul class="header-nav flex items-center justify-around">
            <li class="inline-flex cursor-pointer transition-all py-3 px-6">
                <a class="flex flex-row items-center" href="https://strix.gg/discord">
                    <div class="flex flex-col">
                        <span class="text-base font-bold">Discord Server</span>
                        <div class="text-sm flex flex-row">
                            <member-counter-component></member-counter-component>
                            <span class="ml-1">
                                    Members Online
                                </span>
                        </div>
                    </div>
                    <div class="button button-primary p-3 ml-4 h-12 w-12">
                        <i class="fab fa-discord fa-lg text-primary-700 fa-fw"></i>
                    </div>
                </a>
            </li>
            <li class="inline-flex cursor-pointer py-3 px-6">
                <a href="/">
                    <div class="logo flex">
                        <a href="#" class="h-24 w-24">
                            <img src="{{ asset('static/strix_logo_light.svg') }}" alt=""/>
                        </a>
                    </div>
                </a>
            </li>
            <li class="inline-flex cursor-pointer transition-all py-3 px-6">
                <copy-server-component/>
            </li>
        </ul>
    </div>

    <div class="relative flex items-center bg-neutral-600 flex-wrap py-4 w-full h-full justify-center">
        <ul class="header-nav flex items-center">
            <li class="inline-flex cursor-pointer opacity-75 hover:opacity-100 transition-all py-3 px-6">
                <a href="/">
                    <i class="fad fa-chart-area fa-fw mr-2"></i>
                    <span>Stats</span>
                </a>
            </li>
            <li class="inline-flex cursor-pointer opacity-75 hover:opacity-100 transition-all py-3 px-6">
                <a href="/">
                    <i class="fad fa-lightbulb-on fa-fw mr-2"></i>
                    <span>Suggestions</span>
                </a>
            </li>
            <li class="inline-flex cursor-pointer opacity-75 hover:opacity-100 transition-all py-3 px-6">
                <a href="/">
                    <i class="fad fa-code-merge fa-fw mr-2"></i>
                    <span>Updates</span>
                </a>
            </li>
            <li class="inline-flex cursor-pointer opacity-75 hover:opacity-100 transition-all py-3 px-6">
                <a href="/">
                    <i class="fad fa-shield-check fa-fw mr-2"></i>
                    <span>Staff</span>
                </a>
            </li>

        </ul>
    </div>
</header>
