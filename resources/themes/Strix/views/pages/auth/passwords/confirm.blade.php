@extends('layouts.app')

@section('content')
    <section class="relative flex flex-col py-6">
        <div class="container mx-auto">
            <h3 class="text-3xl font-bold text-neutral-100 text-center">{{ __('Confirm Password') }}</h3>

            {{ __('Please confirm your password before continuing.') }}
            {{ __('We won\'t ask for your password again for a few hours.') }}

            <form method="POST" action="{{ route('password.confirm') }}">
                @csrf

                <div class="flex flex-wrap">
                    <div class="w-full p-3">
                        <label class="form-input-container">
                            <input id="password" type="password" class="form-input form-input-primary @error('password') border-red-500 @enderror" name="password" required autocomplete="current-password">

                            <div class="form-input-label">{{ __('Password') }}</div>

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </label>

                        <div class="w-full p-3 text-left">
                            <div class="flex flex-col mt-3 items-center text-right">
                                <button type="submit" class="button button-primary button-hero">
                                    {{ __('Confirm Password') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="font-medium text-primary-500" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection
