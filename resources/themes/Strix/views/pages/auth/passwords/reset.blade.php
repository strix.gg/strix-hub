@extends('layouts.main')

@section('content')

    <section class="relative flex flex-col py-6">
        <div class="container mx-auto">
            <h3 class="text-3xl font-bold text-neutral-100 text-center">Reset Password</h3>

            <form method="POST" action="{{ route('password.update') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="flex flex-wrap">
                    <div class="w-full p-3">
                        <label class="form-input-container">
                            <input id="email" type="email" name="email"
                                   class="form-input form-input-primary @error('email') border-red-500 @enderror"
                                   placeholder=" "
                                   value="{{ old('email') }}"
                                   required autocomplete="email">
                            <div class="form-input-label">Email Address</div>
                        </label>

                        @error('email')
                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>

                    <div class="w-full p-3">
                        <label class="form-input-container">
                            <input id="password" type="password" name="password"
                                   class="form-input form-input-primary @error('password') border-red-500 @enderror"
                                   value="{{ old('password') }}"
                                   placeholder=" "
                                   required autocomplete="new-password">
                            <div class="form-input-label">New Password</div>
                        </label>

                        @error('password')
                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>

                    <div class="w-full p-3">
                        <label class="form-input-container">
                            <input id="password-confirm" type="password" name="password_confirmation"
                                   class="form-input form-input-primary @error('password') border-red-500 @enderror"
                                   value="{{ old('password') }}"
                                   placeholder=" "
                                   required autocomplete="new-password">
                            <div class="form-input-label">New Password</div>
                        </label>

                        @error('password')
                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>

                    <div class="w-full p-3 text-left">
                        <div class="flex flex-col mt-3 items-center text-right">
                            <button type="submit" class="button button-primary button-hero">Reset Password</button>
                        </div>
                    </div>


                </div>


            </form>
        </div>
    </section>
@endsection
