@extends('layouts.main')

@section('content')

    <section class="relative flex flex-col py-6">
        <div class="container mx-auto">
            <h3 class="text-3xl font-bold text-neutral-100 text-center">Sign in to Strix</h3>

            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="flex flex-wrap">
                    <div class="w-full p-3">
                        <label class="form-input-container">
                            <input id="name" type="text" name="name"
                                   class="form-input form-input-primary @error('name') border-red-500 @enderror"
                                   placeholder=" "
                                   value="{{ old('name') }}"
                                   required autocomplete="name">
                            <div class="form-input-label">Username</div>
                        </label>

                        @error('email')
                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>

                    <div class="w-full p-3">
                        <label class="form-input-container">
                            <input id="password" type="password" name="password"
                                   class="form-input form-input-primary @error('email') border-red-500 @enderror"
                                   placeholder=" "
                                   value="{{ old('password') }}"
                                   required autocomplete="current-password">
                            <span class="form-input-label">Password</span>
                        </label>

                        @error('password')
                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror

                        @if (Route::has('password.request'))
                            <a class="text-primary-500 text-left" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </div>

                    <div class="hidden">
                        <input type="checkbox" name="remember" id="remember" checked>
                    </div>

                    <div class="w-full p-3 text-left">
                        <div class="flex flex-col mt-3 items-center text-right">
                            <button type="submit" class="button button-primary button-hero">Login</button>


                            <a href="{{ route('register') }}" class="text-gray-600">
                                Don't have an account? <span class="font-medium text-primary-500">Sign Up</span>
                            </a>
                        </div>
                    </div>


                </div>

            </form>

        </div>
    </section>
@endsection
