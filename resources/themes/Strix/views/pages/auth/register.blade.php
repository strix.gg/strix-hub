@extends('layouts.main')


@section('content')

    <section class="relative flex flex-col py-6">
        <div class="container mx-auto">
            <h3 class="text-3xl font-bold text-neutral-100 text-center">Get started for free</h3>

            <form method="POST" action="{{ route('register') }}">

                @csrf

                <div class="flex flex-wrap">
                    <div class="w-full p-3">
                        <label class="form-input-container">
                            <input id="name" type="text" name="name"
                                   class="form-input form-input-primary @error('email') border-red-500 @enderror"
                                   value="{{ old('name') }}"
                                   placeholder=" "
                                   required autocomplete="name">
                            <div class="form-input-label">Name</div>
                        </label>

                        @error('name')
                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>

                    <div class="w-full p-3">
                        <label class="form-input-container">
                            <input id="email" type="email" name="email"
                                   class="form-input form-input-primary @error('email') border-red-500 @enderror"
                                   value="{{ old('email') }}"
                                   placeholder=" "
                                   required autocomplete="email">
                            <div class="form-input-label">Email Address</div>
                        </label>

                        @error('email')
                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>

                    <div class="w-full p-3">
                        <label class="form-input-container">
                            <input id="password" type="password" name="password"
                                   class="form-input form-input-primary @error('email') border-red-500 @enderror"
                                   value="{{ old('password') }}"
                                   placeholder=" "
                                   required autocomplete="new-password">
                            <div class="form-input-label">Password</div>
                        </label>

                        @error('password')
                        <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>

                    <div class="w-full p-3">
                        <label class="form-input-container">
                            <input id="password-confirm" type="password" name="password_confirmation"
                                   class="form-input form-input-primary @error('email') border-red-500 @enderror"
                                   value="{{ old('password') }}"
                                   placeholder=" "
                                   required autocomplete="new-password">
                            <div class="form-input-label">Password Confirmation</div>
                        </label>
                    </div>


                    <div class="w-full p-3 text-left">
                        <div class="flex flex-col mt-3 items-center text-right">
                            <button type="submit" class="button button-primary button-hero">Register</button>

                            <a href="{{ route('login') }}" class="text-gray-600">
                                Already have an account? <span class="font-medium text-primary-500">Sign in</span>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
