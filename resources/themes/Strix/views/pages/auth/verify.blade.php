@extends('layouts.main')


@section('content')

    <h3 class="text-3xl font-bold text-black text-center">Please verify your email</h3>

    @if (session('resent'))
        <div class="bg-green-200 text-green-700 rounded p-2 my-4" role="alert">
            {{ __('A fresh verification link has been sent to your email address.') }}
        </div>
    @endif

    <div class="text-gray-600 text-center">
        {{ __('Before proceeding, please check your email for a verification link.') }}
    </div>

    <div class="text-gray-600 text-center mt-4">
        <form method="POST" action="{{ route('verification.resend') }}">
            @csrf
            {{ __('If you did not receive the email') }}, <button type="submit" class="text-primary-500">{{ __('click here to request another') }}</button>.

        </form>
    </div>
@endsection
