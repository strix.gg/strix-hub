@extends('layouts.main')


@section('content')
    <main class="block">
        <section class="user-profile-container">
            <div class="user-profile-banner">
                <div class="background-repeat-none background-position-center-top background-size-cover h-full w-full" style="background-image: url({{ $user->getFirstMediaUrl('cover') }});"></div>
            </div>
            <div class="user-profile-header container mt-10">
                <div class="user-profile-header-content">
                    <div class="user-profile-picture-container">
                        <div class="user-profile-picture-content">
                            <div class="user-profile-picture-image">
                                <a href="{{ route('users.edit', $user) }}">
                                    <img src="{{ $user->getFirstMediaUrl('avatar') }}" alt="profile picture">
                                </a>
                            </div>
                        </div>
                        <div class="user-profile-name-container">
                            <h1 class="user-profile-name-content">
                                <span>{{ $user->name }}</span>
                                <i class="fad fa-badge-check fa-fw"></i>
                            </h1>
                            <p class="user-profile-phrase">
                                {{ $user->tagline ?? $user->name . ' has no tagline :(' }}
                            </p>
                        </div>
                    </div>
                    <a class="button button-neutral" href="{{ route('users.edit', $user) }}">
                        <i class="fad fa-cogs mr-2"></i>
                        Settings
                    </a>
                </div>
            </div>
        </section>

        <section class="mb-10">
            <div class="container flex flex-row flex-1 flex-wrap mx-auto">
                <div class="w-full md:w-1/4 flex flex-col">
                    <div class="flex flex-row w-full">
                        <div class="card w-1/2 m-1">
                            <div class="card-content px-12 py-2">
                                <h1 class="text-neutral-100 text-xl">
                                    <i class="fad fa-minus-circle fa-fw fa-xs"></i>
                                    <span>{{ $user->reputation ?? 0 }}</span>
                                </h1>
                                <p class="text-neutral-300 text-sm">REPUTATION</p>
                            </div>
                        </div>
                        <div class="card w-1/2 m-1">
                            <div class="card-content px-12 py-2">
                                <h1 class="text-green-100 text-xl">{{ $user->stars ?? 0 }}</h1>
                                <p class="text-neutral-300 text-sm">STARS</p>
                            </div>
                        </div>
                    </div>

                    <div class="card m-1">
                        <div class="card-header">
                            <div class="card-header-image-container">
                                <div class="card-header-image blur" style="background-image: url({{ $user->getFirstMediaUrl('cover') }})"></div>
                            </div>
                            <div class="z-10 text-white">
                                <i class="fad fa-user fa-fw mr-1"></i>
                                <span class="font-display-1">User Information</span>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-grid-item">
                                <div class="text-neutral-300 truncate">
                                    Username Changes:
                                </div>
                                <span class="text-neutral-100 truncate text-right">
                                    {{ $user->username_changes ?? 0 }}
                                </span>
                            </div>
                            <div class="card-grid-item">
                                <div class="text-neutral-300 truncate">
                                    Last Visit:
                                </div>

                                @if($online)
                                    <span class="text-green-500 truncate text-right">
                                    <i class="fad fa-circle fa-fw"></i>
                                    Online
                                </span>
                                @else
                                    <span class="text-neutral-100 truncate text-right">
                                    <i class="fad fa-circle fa-fw"></i>
                                    {{ $user->last_visited_at->diffForHumans() }}
                                </span>
                                @endif
                            </div>
                            <div class="card-grid-item">
                                <div class="text-neutral-300 truncate">
                                    Joined:
                                </div>
                                <span class="text-neutral-100 truncate text-right">
                                    {{ $user->created_at->diffForHumans() }}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-full md:w-2/4 flex flex-1 flex-wrap">
                    <div class="card m-1 w-full">
                        <div class="card-header">
                            <div class="card-header-image-container">
                                <div class="card-header-image blur" style="background-image: url({{ $user->getFirstMediaUrl('cover') }})"></div>
                            </div>
                            <div class="z-10 text-white">
                                <i class="fad fa-comments fa-fw mr-1"></i>
                                <span class="font-display-1">Comments</span>
                            </div>
                        </div>
                        <div class="card-content items-start justify-start">
                            <div class="card-grid-item justify-start items-start p-0 h-full">
                                <div class="relative flex flex-col justify-start w-full z-10 p-3">
                                    <a class="relative flex flex-row items-center rounded-full mr-2" style="min-width: 2rem" href="{{ route('users.show', Auth::user()) }}">
                                        <img class="h-8 w-8 rounded-full mr-2" src="{{ Auth::user()->getFirstMediaUrl('avatar') }}" alt="">

                                        <span class="text-white text-base font-display-1 truncate">{{ Auth::user()->name }}</span>
                                    </a>
                                    <div class="flex flex-col w-full">
                                        <span class="text-base break-all">
                                            <form method="POST" action="{{ route('users.comments.store', $user) }}">
                                                @csrf

                                                <div class="flex flex-col items-center">
                                                    <div class="flex-grow text-neutral-200 w-full my-4">
                                                        <comment-editor-component></comment-editor-component>
                                                    </div>
                                                    <button class="relative button button-profile ml-auto" type="submit">
                                                        <div class="button-profile-background-container">
                                                            <div class="button-profile-background-image" style="background-image: url({{ $user->getFirstMediaUrl('cover') }})"></div>
                                                        </div>
                                                        <div class="z-10 text-white">
                                                            <span class="font-bold">Comment</span>
                                                        </div>
                                                    </button>
                                                </div>
                                            </form>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            @foreach($comments as $comment)
                                <div class="card-grid-item justify-start items-start p-0 h-full">
                                    <div class="relative flex flex-col justify-start w-full z-10 p-3">
                                        <a class="relative flex flex-row items-center rounded-full mr-2" style="min-width: 2rem" href="{{ route('users.show', $comment->commenter) }}">
                                            <img class="h-8 w-8 rounded-full mr-2" src="{{ $comment->commenter->getFirstMediaUrl('avatar') }}" alt="">

                                            <span class="text-white text-base font-display-1 truncate">{{ $comment->commenter->name }}</span>

                                            <span class="text-neutral-300 text-right ml-auto">{{ $comment->created_at->diffForHumans() }}</span>
                                        </a>
                                        <div class="flex flex-col w-full">
                                            <span class="text-neutral-200 text-base break-all my-4">
                                                <static-editor-component content="{{ $comment->content }}"></static-editor-component>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="w-full md:w-1/4 flex">
                    <div class="flex flex-col w-full">
                        <div class="card m-1">
                            <div class="card-header">
                                <div class="card-header-image-container">
                                    <div class="card-header-image blur" style="background-image: url({{ $user->getFirstMediaUrl('cover') }})"></div>
                                </div>
                                <div class="z-10 text-white">
                                    <i class="fad fa-users fa-fw mr-1"></i>
                                    <span class="font-display-1">User Groups</span>
                                </div>
                            </div>

                            <div class="card-content items-start justify-start overflow-y-scroll">
                                <div class="block w-full">
                                    <img class="block mx-auto w-auto p-4" src="https://i.imgur.com/OYuAYMq.png" alt="">
                                </div>
                                <div class="block w-full">
                                    <img class="block mx-auto w-auto p-4" src="https://i.imgur.com/OYuAYMq.png" alt="">
                                </div>

                            </div>
                        </div>
                        <div class="card m-1">
                            <div class="card-header">
                                <div class="card-header-image-container">
                                    <div class="card-header-image blur" style="background-image: url({{ $user->getFirstMediaUrl('cover') }})"></div>
                                </div>
                                <div class="z-10 text-white">
                                    <i class="fad fa-clipboard fa-fw mr-1"></i>
                                    <span class="font-display-1">Recent Threads</span>
                                </div>
                            </div>

                            <div class="card-content">
                                @foreach($threads as $thread)
                                    <div class="card-grid-item">
                                        <a class="text-neutral-100 truncate max-w-xxs mr-2" href="{{ route('forums.thread.show', [$thread, $thread->slug]) }}">
                                            {{ $thread->title }}
                                        </a>
                                        <span class="text-neutral-300 truncate text-right">
                                    {{ $thread->created_at->diffForHumans() }}
                                </span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </main>
@endsection
