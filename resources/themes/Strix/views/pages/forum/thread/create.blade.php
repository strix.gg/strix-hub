@extends('layouts.main')

@section('content')
    <main class="block">
        <section class="relative flex flex-col flex-1 items-center p-6 mb-10">
            <div class="container flex flex-row flex-wrap mx-auto">
                <div class="w-full flex flex-wrap z-10">
                    <div class="card m-1 w-full">
                        <div class="card-content items-start justify-start p-4">
                            <form method="POST" class="w-full" action="{{ route('forums.thread.store', $board) }}">
                                @csrf

                                <div class="w-full my-2 p-3">
                                    <label class="form-input-container">
                                        <input id="title" type="text" name="title"
                                               class="form-input form-input-primary @error('title') border-red-500 @enderror"
                                               value="{{ old('title') }}"
                                               placeholder=" "
                                               required autocomplete="title">
                                        <div class="form-input-label">title</div>
                                    </label>

                                    @error('name')
                                    <span class="mt-2 font-medium text-red-700 text-sm" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>

                                <div class="w-full my-2 p-3">
                                    <div class="flex items-center">
                                        <div class="flex-grow text-neutral-200 w-full my-4">
                                            <comment-editor-component></comment-editor-component>
                                        </div>
                                    </div>
                                </div>

                                <div class="w-full p-3 text-left">
                                    <div class="flex flex-col items-end">
                                        <button type="submit" class="button button-primary button-hero mx-0 my-2">Create Thread</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
