@extends('layouts.main')

@section('content')
    <main class="block">


        <section class="relative flex flex-col flex-1 items-center p-6 pb-0">
            <div class="user-profile-banner">
                <div class="background-repeat-none background-position-center-top background-size-cover h-full w-full"
                     style="background-image: url({{ $thread->user->getFirstMediaUrl('cover') }})">
                </div>
            </div>

            <div class="container flex flex-row flex-wrap mx-auto">
                <div class="relative w-full flex flex-wrap z-10">
                    <div class="relative w-full rounded py-12 flex items-center">
                        <h4 class="text-neutral-100 font-display-1 text-3xl">
                            {{ $thread->title }}
                        </h4>

                        <div class="ml-auto">
                            <button class="button button-secondary">Reply</button>
                        </div>
                    </div>

                    <div class="ml-auto mb-4">
                        {{ $comments->links() }}
                    </div>

                    @if($comments->currentPage() == 1)
                        <article class="card m-1 w-full">
                            <div class="card-content flex-row items-start justify-start">
                                <div class="block relative w-full md:w-64 p-4 align-top overflow-hidden h-full">
                                    <div class="absolute top-0 left-0 h-full w-full">
                                        <img class="relative object-cover object-center w-full opacity-75" style="height: 400px" src="{{ $thread->user->getFirstMediaUrl('postbit_cover') }}" alt="">
                                        <div class="absolute inset-0 bg-cover" style="background-image: linear-gradient(rgba(25,28,31, 0.5) 0px, #191C1F 400px);"></div>
                                    </div>
                                    <div class="relative z-10">
                                        <div class="text-center mb-1">
                                            <h4 class="font-display-1 font-bold text-xl mb-1 text-white">{{ $thread->user->name }}</h4>
                                            <a class="inline-block rounded-full h-24 w-24 overflow-hidden" href="{{ route('users.show', $thread->user) }}">
                                                <img class="block h-full w-full truncate" src="{{ $thread->user->getFirstMediaUrl('avatar') }}" alt="">
                                            </a>
                                        </div>
                                        <div class="text-center text-sm">
                                            <p class="text-neutral-200 mb-4">{{ $thread->user->tagline }}</p>
                                            <div>
                                                <img class="m-auto" src="https://i.imgur.com/OYuAYMq.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="block w-full min-w-0 align-top">
                                    <header class="flex p-4 items-center border-b border-neutral-500">
                                        <div class="text-neutral-200">
                                            {{ $thread->created_at->diffForHumans() }}
                                        </div>
                                        <div class="ml-auto text-neutral-200">
                                            <a href="#{{$thread->id}}">
                                                #{{ \Str::limit($thread->id, 8, '') }}
                                            </a>
                                        </div>
                                        <div class="hidden ml-2">
                                            <button>
                                                <i class="fad fa-times-circle fa-fw text-red-500"></i>
                                            </button>
                                        </div>
                                    </header>
                                    <div class="flex flex-col p-4 text-neutral-100" style="height: calc(100% - 55px)">
                                        <static-editor-component content="{{ $thread->content }}"></static-editor-component>
                                        <footer class="block mt-auto">
                                            <div class="flex items-center">
                                                <div class="ml-auto">
                                                    <div class="inline-block">
                                                        <button class="button button-secondary">
                                                            Edit
                                                        </button>
                                                    </div>
                                                    <div class="inline-block">
                                                        <button class="button button-secondary">
                                                            Reply
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                        </article>
                    @endif
                </div>
            </div>
        </section>

        <section class="relative flex flex-col flex-1 items-center p-6 mb-10">
            <div class="container flex flex-row flex-wrap mx-auto">
                @foreach($comments as $comment)
                    <article id="{{ $comment->id }}" class="card m-1 w-full">
                        <div class="card-content flex-row items-start justify-start">
                            <div class="block relative w-full md:w-64 p-4 align-top overflow-hidden h-full">
                                <div class="absolute top-0 left-0 h-full w-full">
                                    <img class="relative object-cover object-center w-full opacity-75" style="height: 400px" src="{{ $comment->commenter->getFirstMediaUrl('postbit_cover') }}" alt="">
                                    <div class="absolute inset-0 bg-cover" style="background-image: linear-gradient(rgba(25,28,31, 0.5) 0px, #191C1F 400px);"></div>
                                </div>
                                <div class="relative z-10">
                                    <div class="text-center mb-1">
                                        <h4 class="font-display-1 font-bold text-xl mb-1 text-white">{{ $comment->commenter->name }}</h4>
                                        <a class="inline-block rounded-full h-24 w-24 overflow-hidden" href="{{ route('users.show', $comment->commenter)  }}">
                                            <img class="block h-full w-full truncate" src="{{ $comment->commenter->getFirstMediaUrl('avatar') }}" alt="">
                                        </a>
                                    </div>
                                    <div class="text-center text-sm">
                                        <p class="text-neutral-200 mb-4">{{ $comment->commenter->tagline }}</p>
                                        <div>
                                            <img class="m-auto" src="https://i.imgur.com/OYuAYMq.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block w-full min-w-0 align-top h-full">
                                <header class="flex p-4 items-center border-b border-neutral-500">
                                    <div class="text-neutral-200">
                                        {{ $comment->created_at->diffForHumans() }}
                                    </div>
                                    <div class="ml-auto text-neutral-200">
                                        <a href="#{{$comment->id}}">
                                            #{{ \Str::limit($comment->id, 8, '') }}
                                        </a>
                                    </div>
                                    <div class="hidden ml-2">
                                        <button>
                                            <i class="fad fa-times-circle fa-fw text-red-500"></i>
                                        </button>
                                    </div>
                                </header>
                                <div class="flex flex-col p-4 text-neutral-100" style="height: calc(100% - 55px)">
                                    <static-editor-component content="{{ $comment->content }}"></static-editor-component>
                                    <footer class="block mt-auto">
                                        <div class="flex items-center">
                                            <div class="ml-auto">
                                                <div class="inline-block">
                                                    <button class="button button-secondary">
                                                        Edit
                                                    </button>
                                                </div>
                                                <div class="inline-block">
                                                    <button class="button button-secondary">
                                                        Reply
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                        </div>
                    </article>
                @endforeach


                <article class="card m-1 w-full">
                    <div class="card-content flex-row items-start justify-start">
                        <div class="block w-full min-w-0 align-top">
                            <div class="flex flex-col p-4 text-neutral-100" style="height: calc(100% - 55px)">
                                <form method="POST" action="{{ route('forums.thread.comment.store', [$thread]) }}">
                                    @csrf

                                    <div class="flex flex-col items-center">
                                        <div class="flex-grow text-neutral-200 w-full my-4">
                                            <comment-editor-component></comment-editor-component>
                                        </div>
                                        <button class="relative button button-primary ml-auto" type="submit">
                                            Create Comment
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </article>

                <div class="ml-auto mt-4">
                    {{ $comments->links() }}
                </div>
            </div>
        </section>
    </main>
@endsection
