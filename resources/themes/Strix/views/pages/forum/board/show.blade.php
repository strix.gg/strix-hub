@extends('layouts.main')

@section('content')
    <main class="block">
        <section class="relative flex flex-col flex-1 items-center p-6 mb-10">
            <div class="user-profile-banner">
                <div class="background-repeat-none background-position-center-top background-size-cover h-full w-full" style="background-image: url({{ Auth::user()->getFirstMediaUrl('cover') }});"></div>
            </div>
            <div class="container flex flex-row flex-wrap mx-auto">
                <div class="w-full flex flex-wrap z-10">
                    <div class="py-12 px-32 text-center justify-center w-full">
                        <h3 class="text-neutral-100 font-display-1 font-bold text-xl">
                            {{ $board->title }}
                        </h3>
                    </div>

                    <a href="{{ route('forums.thread.create', $board) }}" class="button button-primary button-hero ml-auto">
                        Create Thread
                    </a>

                    <div class="card m-1 w-full">
                        <div class="card-content items-start justify-start">
                            @foreach($threads as $thread)
                                <div class="block card-grid-item justify-start items-start p-0 h-full">
                                    <div class="table table-fixed w-full h-full">
                                        <div class="table-cell w-20 p-2 pr-0 text-center align-middle">
                                            <a href={{ route('users.show', $thread->user) }}#">
                                                <img class="w-12 h-12 align-middle m-auto rounded-full" src="{{ $thread->user->getFirstMediaUrl('avatar') }}" alt="">
                                            </a>
                                        </div>
                                        <div class="table-cell p-2 align-middle text-white text-sm">
                                            <h3 class="text-base truncate">
                                                <a class="text-neutral-100" href="{{ route('forums.thread.show', [$thread, $thread->slug]) }}">
                                                    {{ $thread->title }}
                                                </a>
                                            </h3>
                                            <span>
                                                <a class="text-primary-500" href="{{ route('users.show', $thread->user) }}">
                                                    {{ $thread->user->name }}
                                                </a>
                                        </span>
                                        </div>
                                        <div class="table-cell p-2 align-middle text-white text-sm text-left w-40">
                                            <div class="text-neutral-300">
                                                <i class="fad fa-fw fa-comments"></i>
                                                <span class="text-white text-right">{{ $thread->comment_count }}</span>
                                            </div>
                                            <div class="text-neutral-300">
                                                <i class="fad fa-fw fa-clock"></i>
                                                <span class="text-white text-right">{{ $thread->created_at->diffForHumans() }}</span>
                                            </div>
                                        </div>
                                        @if($thread->comments()->exists())
                                            <div class="relative hidden md:table-cell w-80 p-2 align-middle text-white overflow-hidden text-right">
                                                <div class="float-right ml-2 bg-cover bg-center relative w-full">
                                                    <img src="{{ $thread->comments->last()->commenter->getFirstMediaUrl('avatar') }}" alt=""
                                                         style="
                                                height: 140px;
                                                width: 140px;
                                                position: absolute;
                                                top: -40px;
                                                right: -40px;
                                                border-radius: 100%;
                                                opacity: .15;
                                            ">
                                                </div>
                                                <div class="relative truncate z-20">
                                                    <a class="text-primary-500" href="{{ route('forums.thread.show', [$thread, $thread->slug]) }}">
                                                        {{ $thread->comments->last()->commenter->name }}
                                                    </a>
                                                    <div class="block">
                                                        <span>{{ $thread->comments->last()->created_at->diffForHumans() }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                        @else
                                            <div class="relative hidden md:table-cell w-80 p-2 align-middle text-white overflow-hidden text-right">
                                                <div class="relative truncate z-20 text-neutral-100">
                                                    No Comments yet :(
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </main>
@endsection
