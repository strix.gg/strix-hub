@extends('layouts.main')

@section('content')
    <main class="block">
        <section class="relative flex flex-col flex-1 items-center p-6 mb-10">
            <div class="container flex flex-row flex-wrap mx-auto">
                <div class="w-full md:w-3/4 px-2">
                    @foreach($categories as $category)
                        <div class="card m-1 w-full">
                            <div class="card-header">
                                <div class="card-header-image-container">
                                    <div class="card-header-image" style="background-image: url({{ Auth::user()->getFirstMediaUrl('cover') }})"></div>
                                </div>
                                <div class="z-10 text-white">
                                    <span class="font-display-1 text-lg">{{ $category->title }}</span>
                                </div>
                            </div>
                            <div class="card-content items-start justify-start">
                                @foreach($category->boards as $board)
                                    <div class="block card-grid-item justify-start items-start p-0 h-full">
                                        <div class="table table-fixed w-full h-full">
                                            <div class="table-cell w-20 p-2 pr-0 text-center align-middle">
                                                <a class="" href="#">
                                                    <img class="w-12 h-12 align-middle m-auto" src="https://media.gmodstore.com/_/assets/img/forum/icons/megaphone.svg" alt="">
                                                </a>
                                            </div>
                                            <div class="table-cell p-2 align-middle text-white text-sm">
                                                <h3 class="text-base">
                                                    <a href="{{ route('forums.board.show', $board) }}">{{ $board->title }}</a>
                                                </h3>
                                                <span class="text-neutral-200">Threads:</span>
                                                {{ $board->thread_count }}
                                                <span class="text-neutral-200">Comments:</span>
                                                {{ $board->comment_count }}
                                            </div>
                                            <div class="relative hidden md:table-cell w-80 p-2 align-middle text-white overflow-hidden text-left">
                                                <div class="float-right ml-2 bg-cover bg-center relative w-full">
                                                    <img src="{{ $board->threads->last()->user->getFirstMediaUrl('avatar') }}" alt=""
                                                         style="
                                                height: 140px;
                                                width: 140px;
                                                position: absolute;
                                                top: -40px;
                                                right: -40px;
                                                border-radius: 100%;
                                                opacity: .15;
                                            ">
                                                </div>
                                                <div class="truncate z-10">
                                                    <a class="text-primary-500" href="{{ route('forums.thread.show', [$board->threads->last(), $board->threads->last()->slug]) }}">
                                                       {{ $board->threads->last()->title }}
                                                    </a>
                                                </div>
                                                <div class="truncate z-10">
                                                    <div class="inline-block">
                                                        <span>{{ $board->threads->last()->created_at->diffForHumans() }}</span>
                                                    </div>
                                                    |
                                                    <a class="text-primary-500" href="{{ route('users.show', $board->threads->last()->user) }}">
                                                        {{ $board->threads->last()->user->name }}
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="w-full md:w-1/4 px-2">
                    <div class="card m-1 w-full">
                        <div class="card-header">
                            <div class="z-10 text-white">
                                <span class="font-display-1 text-lg">Latest Threads</span>
                            </div>
                        </div>
                        <div class="card-content items-start justify-start">
                            @foreach($threads as $thread)
                                <div class="block card-grid-item justify-start items-start p-0 h-full">
                                    <div class="table table-fixed w-full h-full">
                                        <div class="table-cell w-20 p-2 pr-0 text-center align-middle">
                                            <a href="{{ route('users.show', $thread->user) }}">
                                                <img class="w-12 h-12 align-middle m-auto rounded-full" src="{{ $thread->user->getFirstMediaUrl('avatar') }}" alt="">
                                            </a>
                                        </div>
                                        <div class="table-cell p-2 align-middle text-white text-sm">
                                            <h3 class="text-base truncate">
                                                <a class="text-primary-500" href="{{ route('forums.thread.show', [$thread, $thread->slug]) }}">
                                                    {{ $thread->title }}
                                                </a>
                                            </h3>
                                            <span>
                                            <span>{{ $thread->created_at->diffForHumans() }}</span>
                                            by
                                            <span>
                                                <a class="text-primary-500" href="{{ route('users.show', $thread->user) }}">
                                                    {{ $thread->user->name }}
                                                </a>
                                            </span>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </main>
@endsection
