@extends('layouts.main')

@section('content')
    <main class="block">
        <section class="relative flex flex-col py-6">

            <div class='relative container p-6 m-auto neutral flex flex-col mx-auto justify-center items-center'>
                <div class="flex flex-col pt-6 md:pt-0 w-2/3">
                    <h1 class="text-center text-3xl md:text-5xl text-neutral-100 font-black mb-8 font-display-1">Join us on the next big journey.</h1>

                    <p class="text-center text-md md:text-xl text-neutral-200 leading-normal mb-10">
                        Join us and write our history with ever changing content with the backbone of RLCraft. It's time for a fresh start. Let's grow together!
                    </p>

                    <div class="flex flex-col md:flex-row items-center text-center justify-center text-base h-12 font-display-1">

                        <copy-server-main-component/>

                        <a href="https://strix.gg/discord" class="button button-secondary button-hero text-neutral-100">
                            Discord
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="relative flex flex-col py-6">
            <div class="container mx-auto max-w-6xl flex flex-col md:flex-row items-stretch">
                <div class="card w-full md:w-1/3 m-2">
                    <div class="card-banner-container">
                        <div class="card-banner-image">
                            <div class="card-background-preflight blur-light card-background-mask" style="background-image: url({{ asset('static/minecraft/backgrounds/mc_5.png') }})"></div>
                        </div>
                    </div>
                    <div class="card-content px-6 py-16 z-50 text-neutral-100">
                        <i class="fad fa-swords fa-fw fa-2x"></i>
                        <span class="font-bold mt-4">Fight along with friends</span>
                    </div>
                </div>
                <div class="card w-full md:w-1/3 m-2">
                    <div class="card-banner-container">
                        <div class="card-banner-image">
                            <div class="card-background-preflight blur-light card-background-mask" style="background-image: url({{ asset('static/minecraft/backgrounds/mc_1.png') }})"></div>
                        </div>
                    </div>
                    <div class="card-content px-6 py-16 z-50 text-neutral-100">
                        <i class="fad fa-chess-rook-alt fa-fw fa-2x"></i>
                        <span class="font-bold mt-4"> Exclusive Content</span>
                    </div>
                </div>
                <div class="card w-full md:w-1/3 m-2">
                    <div class="card-banner-container">
                        <div class="card-banner-image">
                            <div class="card-background-preflight blur-light card-background-mask" style="background-image: url({{ asset('static/minecraft/backgrounds/mc_2.png') }})"></div>
                        </div>
                    </div>
                    <div class="card-content px-6 py-16 z-50 text-neutral-100">
                        <i class="fad fa-check-circle fa-fw fa-2x"></i>
                        <span class="font-bold mt-4">Consistent Updates</span>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
