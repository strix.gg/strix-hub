<!DOCTYPE html>
<html class="bg-neutral-500" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>
    <meta name="title" content="Strix - Optimized rlcraft"/>
    <meta name="description" content="Optimized rlcraft "/>
    <meta name="keywords"
          content="Minecraft, Minecraft Server, Minecraft Forge, RLcraft, RL craft, rl craft, rlcraft minecraft curseforge "/>
    <meta name="robots" content="index, follow"/>
    <meta name="language" content="English"/>
    <meta name="revisit-after" content="3 days"/>
    <meta name="author" content="Strix" />
    <meta property="og:site_name" content="Strix- Optimized rlcraft" />
    <meta property="og:title" content="Strix - Optimized rlcraft"/>
    <meta property="og:description"
          content="Optimized rlcraft"/>
    <meta property="og:url" content="https://strix.gg"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">



    <title>Strix</title>

    <!-- Styles -->
    {!! Theme::css('css/app.css?v={cache-version}') !!}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:700,800,900&display=swap" rel="stylesheet">
</head>
<body id="body" style="@yield('body_class')">

<div class="font-sans font-normal antialiased">
    <div class="relative h-full" id="app">
        <div class="header relative bg-transparent w-full z-10 overflow-hidden">

            @include('partials.header')

        </div>
        @yield('content')
    </div>
</div>

<!-- Scripts -->
{!! Theme::js('js/app.js?v={cache-version}') !!}
</body>
</html>
