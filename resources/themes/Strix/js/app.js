/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../../Core/js/core.js');

window.Vue = require('vue');


import VueClipboard from 'vue-clipboard2';

Vue.use(VueClipboard);

Vue.prototype.$modal = {
    show(name, params = {}) {
        location.hash = name;
    },

    hide(name) {
        location.hash = '#';
    },
};

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('player-counter-component', require('./components/PlayerCounterComponent.vue').default);
Vue.component('member-counter-component', require('./components/MemberCounterComponent.vue').default);
Vue.component('copy-server-main-component', require('./components/partials/CopyServerMainComponent.vue').default);
Vue.component('copy-server-component', require('./components/partials/CopyServerComponent.vue').default);
Vue.component('comment-editor-component', require('./components/editor/CommentEditorComponent').default);
Vue.component('static-editor-component', require('./components/editor/StaticEditorComponent').default);
Vue.component('tab-component', require('./components/tabs/TabComponent').default);
Vue.component('tabs-component', require('./components/tabs/TabsComponent').default);
Vue.component('profile-tab-component', require('./components/profile/TabComponent').default);
Vue.component('profile-tabs-component', require('./components/profile/TabsComponent').default);
Vue.component('modal-component', require('./components/ModalComponent').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

