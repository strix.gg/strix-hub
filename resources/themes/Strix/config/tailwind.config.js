module.exports = {
    important: true,
    theme: {
        fontFamily: {
            sans: [
                'Inter',
                '-apple-system',
                'BlinkMacSystemFont',
                '"Segoe UI"',
                'Roboto',
                '"Helvetica Neue"',
                'Arial',
                '"Noto Sans"',
                'sans-serif',
                '"Apple Color Emoji"',
                '"Segoe UI Emoji"',
                '"Segoe UI Symbol"',
                '"Noto Color Emoji"',
            ],
            serif: [
                'Georgia',
                'Cambria',
                '"Times New Roman"',
                'Times',
                'serif',
            ],
            mono: [
                'Menlo',
                'Monaco',
                'Consolas',
                '"Liberation Mono"',
                '"Courier New"',
                'monospace',
            ],
        },
        extend: {
            width: theme => ({
                80: '20rem',
                160: '40rem',
            }),
            colors: {
                primary: {
                    default: '#F5C400',
                    '100': '#FFF3C2',
                    '200': '#FFE98F',
                    '300': '#FFDE5C',
                    '400': '#FFD429',
                    '500': '#F5C400',
                    '600': '#C29B00',
                    '700': '#8F7200',
                    '800': '#5C4900',
                    '900': '#292100'
                },
                neutral: {
                    default: '#202427',
                    '100': '#EEF0F1',
                    '200': '#B6BEC3',
                    '300': '#7E8B95',
                    '400': '#4E585F',
                    '500': '#202427',
                    '600': '#191C1F',
                    '700': '#121516',
                    '800': '#0B0D0E',
                    '900': '#050506'
                }
            }
        }
    },
    variants: {},
    plugins: [
        require('@tailwindcss/custom-forms')
    ]
}
