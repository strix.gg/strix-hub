<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $slug
 * @property string|null $tagline
 * @property int $reputation
 * @property int $stars
 * @property int $username_changes
 * @property int|null $credits
 * @property int|null $steam_account_id
 * @property \Illuminate\Support\Carbon|null $last_visited_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $userComments
 * @property-read int|null $user_comments_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCredits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastVisitedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereReputation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereStars($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSteamAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereTagline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsernameChanges($value)
 * @property int|null $preferred_role_id
 * @property-read \App\Models\Color $color
 * @property-read \App\Models\Credit $credit
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Forum\Thread[] $threads
 * @property-read int|null $threads_count
 * @property-read \App\Models\Color $userColor
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePreferredRoleId($value)
 */
	class User extends \Eloquent {}
}

namespace App\Models\Payment{
/**
 * App\Models\Payment\Invoice
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payment\Invoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payment\Invoice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payment\Invoice query()
 */
	class Invoice extends \Eloquent {}
}

namespace App\Models\Store{
/**
 * App\Models\Store\Product
 *
 * @property string $id
 * @property string $title
 * @property int $base_amount
 * @property int $bonus_amount
 * @property int $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Store\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Store\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Store\Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Store\Product whereBaseAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Store\Product whereBonusAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Store\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Store\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Store\Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Store\Product whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Store\Product whereUpdatedAt($value)
 */
	class Product extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Comment
 *
 * @property string $id
 * @property string|null $commenter_id
 * @property string|null $commenter_type
 * @property string $commentable_type
 * @property string $commentable_id
 * @property array $content
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $commentable
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $commenter
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereCommentableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereCommentableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereCommenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereCommenterType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Comment extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Permission
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Permission permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Permission role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereUpdatedAt($value)
 */
	class Permission extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Media
 *
 * @property string $id
 * @property string $model_id
 * @property string $model_type
 * @property string $collection_name
 * @property string $name
 * @property string $file_name
 * @property string|null $mime_type
 * @property string $disk
 * @property int $size
 * @property array $manipulations
 * @property array $custom_properties
 * @property array $responsive_images
 * @property int|null $order_column
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $extension
 * @property-read mixed $human_readable_size
 * @property-read mixed $type
 * @property-read \App\Models\Media $model
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\MediaLibrary\Models\Media ordered()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereCollectionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereCustomProperties($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereDisk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereManipulations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereMimeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereModelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereModelType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereOrderColumn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereResponsiveImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $url
 */
	class Media extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Credit
 *
 * @property string $id
 * @property int $user_id
 * @property int $balance
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credit query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credit whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credit whereUserId($value)
 */
	class Credit extends \Eloquent {}
}

namespace App\Models\Forum{
/**
 * App\Models\Forum\Board
 *
 * @property string $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $category_id
 * @property int $locked
 * @property int $private
 * @property int $thread_count
 * @property int $comment_count
 * @property int $weight
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Forum\Board $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Forum\Thread[] $threads
 * @property-read int|null $threads_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Board onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereCommentCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereLocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board wherePrivate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereThreadCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Board withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Board withoutTrashed()
 * @mixin \Eloquent
 */
	class Board extends \Eloquent {}
}

namespace App\Models\Forum{
/**
 * App\Models\Forum\Thread
 *
 * @property string $id
 * @property string $title
 * @property string $slug
 * @property int $comment_count
 * @property string $content
 * @property string $board_id
 * @property string $user_id
 * @property int $locked
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Forum\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \App\Models\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Thread onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereBoardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereCommentCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereLocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Thread withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Thread withoutTrashed()
 * @mixin \Eloquent
 * @property string $uid
 * @property-read \App\Models\Forum\Board $board
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereUid($value)
 */
	class Thread extends \Eloquent {}
}

namespace App\Models\Forum{
/**
 * App\Models\Forum\Category
 *
 * @property string $id
 * @property string $title
 * @property string $slug
 * @property string|null $description
 * @property int $locked
 * @property int $private
 * @property int $weight
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Forum\Board[] $boards
 * @property-read int|null $boards_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Category onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereLocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category wherePrivate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Category withoutTrashed()
 * @mixin \Eloquent
 */
	class Category extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Color
 *
 * @property string $id
 * @property string $color
 * @property string $colorer_id
 * @property string|null $colorer_type
 * @property string $colorable_type
 * @property string $colorable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Color $colorable
 * @property-read \App\Models\Color $colorer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Color newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Color newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Color query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Color whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Color whereColorableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Color whereColorableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Color whereColorerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Color whereColorerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Color whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Color whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Color whereUpdatedAt($value)
 */
	class Color extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Role
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Role permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Changelog
 *
 * @property string $id
 * @property string $title
 * @property string $content
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Changelog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Changelog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Changelog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Changelog whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Changelog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Changelog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Changelog whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Changelog whereUpdatedAt($value)
 */
	class Changelog extends \Eloquent {}
}

