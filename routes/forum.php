<?php

Route::prefix('forums')->middleware('auth')->name('forums.')->group(function() {
    Route::get('/', [\App\Http\Controllers\Forum\ForumController::class, '__invoke'])->name('index');

    Route::resource('category', \App\Http\Controllers\Forum\Category\CategoryController::class)->except([
        'create', 'store', 'edit', 'delete'
    ]);
//    Route::patch('category/{category}/media', [\App\Http\Controllers\Forum\Category\CategoryMediaController::class, 'update'])->name('category.media.update');


    Route::resource('board', \App\Http\Controllers\Forum\Board\BoardController::class)->except([
        'create', 'store', 'edit', 'delete'
    ]);
//    Route::patch('board/{board}/media', [\App\Http\Controllers\Forum\Board\BoardMediaController::class, 'update'])->name('board.media.update');

    Route::resource('thread', \App\Http\Controllers\Forum\Thread\ThreadController::class)->except([
        'create', 'store', 'show'
    ]);

    Route::get('{board}/thread/create', [\App\Http\Controllers\Forum\Thread\ThreadController::class, 'create'])->name('thread.create');
    Route::post('{board}/thread', [\App\Http\Controllers\Forum\Thread\ThreadController::class, 'store'])->name('thread.store');

    Route::get('thread/{thread}/{slug?}', [\App\Http\Controllers\Forum\Thread\ThreadController::class, 'show'])->name('thread.show');

    Route::post('thread/{thread}/comment', [\App\Http\Controllers\Forum\Thread\CommentsController::class, 'store'])
        ->name('thread.comment.store');

    Route::get('comment/{comment}', [\App\Http\Controllers\Forum\Thread\CommentsController::class, 'edit'])
        ->name('comment.edit');

    Route::patch('comment/{comment}', [\App\Http\Controllers\Forum\Thread\CommentsController::class, 'update'])
        ->name('comment.update');

});

