<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('/minecraft/players/count', \App\Http\Controllers\Api\Minecraft\ServerQueryController::class);
Route::get('/discord/members/count', \App\Http\Controllers\Api\Minecraft\ServerQueryController::class);
Route::get('/users', [\App\Http\Controllers\Api\Users\UsersController::class, 'index']);
Route::get('/users/{user}', [\App\Http\Controllers\Api\Users\UsersController::class, 'show']);
Route::get('/changelog/actions/latest', [\App\Http\Controllers\Api\Changelog\Actions\LatestChangelog::class, '__invoke']);
