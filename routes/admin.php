<?php

Route::prefix('admin')->middleware(['auth', 'checkIfAdmin'])->name('admin.')->group(function() {
    Route::resource('users', \App\Http\Controllers\Admin\Users\UsersController::class);
    Route::patch('users/{user}/roles', [\App\Http\Controllers\Admin\Users\UsersRoleController::class, 'update'])->name('users.roles.update');

    Route::resource('category', \App\Http\Controllers\Admin\Forum\CategoryController::class);
    Route::patch('category/{category}/media', [\App\Http\Controllers\Admin\Forum\CategoryMediaController::class, 'update'])->name('category.media.update');

    Route::resource('board', \App\Http\Controllers\Admin\Forum\BoardController::class);
    Route::patch('board/{board}/media', [\App\Http\Controllers\Admin\Forum\BoardMediaController::class, 'update'])->name('board.media.update');

    Route::resource('role', \App\Http\Controllers\Admin\Roles\RoleController::class);
    Route::patch('role/{role}/media', [\App\Http\Controllers\Admin\Roles\RoleMediaController::class, 'update'])->name('role.media.update');

    Route::resource('changelog', \App\Http\Controllers\Admin\Changelogs\ChangelogController::class);
    Route::patch('changelog/{changelog}/media', [\App\Http\Controllers\Admin\Changelogs\ChangelogMediaController::class, 'update'])->name('changelog.media.update');
});
