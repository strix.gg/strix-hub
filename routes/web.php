<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/test', function(){
//
//    dd(\Illuminate\Support\Str::uuid()->toString(),
//        \Illuminate\Support\Str::orderedUuid()->toString());
//
//});

Route::get('/', [\App\Http\Controllers\Base\IndexController::class, '__invoke'])->name('index');

Route::get('discord', [\App\Http\Controllers\Redirects\DiscordController::class, '__invoke'])->name('redirects.discord');
Route::get('steam', [\App\Http\Controllers\Redirects\SteamController::class, '__invoke'])->name('redirects.steam.group');

Route::resource('users', \App\Http\Controllers\User\UserController::class);

Route::prefix('users')->name('users.')->group(function() {

    Route::patch('{user}/media', [\App\Http\Controllers\User\UserMediaController::class, 'update'])->name('media.update');
    Route::patch('{user}/role', [\App\Http\Controllers\User\UserRoleController::class, 'update'])->name('role.update');
    Route::post('{user}/color', [\App\Http\Controllers\User\UserColorController::class, 'store'])->name('color.store');
    Route::patch('{user}/{color}', [\App\Http\Controllers\User\UserColorController::class, 'update'])->name('color.update');
    Route::post('{user}/comment', [\App\Http\Controllers\User\CommentsController::class, 'store'])->name('comments.store');
});
