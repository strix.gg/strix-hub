<?php

Route::post('/payment/gateway/paypal/express/{credit}', [\App\Http\Controllers\Payment\Gateway\Paypal\ExpressCheckoutController::class, 'getExpressCheckout'])
    ->name('payment.gateway.paypal.express');

Route::get('/payment/gateway/paypal/express/{user}/{credit}/success', [\App\Http\Controllers\Payment\Gateway\Paypal\ExpressCheckoutController::class, 'getExpressCheckoutSuccess'])
    ->name('payment.gateway.paypal.express.success');

Route::post('/payment/gateway/paypal/ipn', [\App\Http\Controllers\Payment\Gateway\Paypal\IpnHandler::class, 'handle']);
