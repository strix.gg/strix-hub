<?php

namespace App\Projectors\User\Credits;

use App\Events\User\Credit\CreditsAdded;
use App\Events\User\Credit\CreditsSubtracted;
use App\Models\Credit;
use Spatie\EventSourcing\Projectors\ProjectsEvents;
use Spatie\EventSourcing\Projectors\QueuedProjector;

final class UserCreditsProjector implements QueuedProjector
{
    use ProjectsEvents;

    /**
     * @param CreditsAdded $event
     */
    public function onCreditsAdded(CreditsAdded $event)
    {
        $credit = Credit::where('id', '=', $event->modelUuid)->first();

        $credit->increment('balance', $event->amount);
    }

    /**
     * @param CreditsSubtracted $event
     */
    public function onCreditsSubtracted(CreditsSubtracted $event)
    {
        $credit = Credit::where('id', '=', $event->modelUuid)->first();

        $credit->decrement('balance', $event->amount);
    }
}
