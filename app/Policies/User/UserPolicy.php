<?php

namespace App\Policies\User;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $auth)
    {
        return $auth->can('view user');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function view(User $auth, User $user)
    {
        return $auth->can('view user');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $auth)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function update(User $auth, User $user)
    {
        if ($auth->is($user)){
            return true;
        }

        return $auth->can('edit user');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $auth
     * @param \App\Models\User $user
     * @return mixed
     */
    public function delete(User $auth, User $user)
    {
        return $auth->can('delete user');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User $auth
     * @param \App\Models\User $user
     * @return mixed
     */
    public function restore(User $auth, User $user)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User $auth
     * @param \App\Models\User $user
     * @return mixed
     */
    public function forceDelete(User $auth, User $user)
    {
        //
    }
}
