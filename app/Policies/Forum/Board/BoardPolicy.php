<?php

namespace App\Policies\Forum\Board;

use App\Models\Forum\Board;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BoardPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any boards.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('view board');
    }

    /**
     * Determine whether the user can view the board.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forum\Board  $board
     * @return mixed
     */
    public function view(User $user, Board $board)
    {
        return $user->can('view board');
    }

    /**
     * Determine whether the user can create boards.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('create board');
    }

    /**
     * Determine whether the user can update the board.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forum\Board  $board
     * @return mixed
     */
    public function update(User $user, Board $board)
    {
        return $user->can('update board');
    }

    /**
     * Determine whether the user can delete the board.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forum\Board  $board
     * @return mixed
     */
    public function delete(User $user, Board $board)
    {
        return $user->can('delete board');
    }

    /**
     * Determine whether the user can restore the board.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forum\Board  $board
     * @return mixed
     */
    public function restore(User $user, Board $board)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the board.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forum\Board  $board
     * @return mixed
     */
    public function forceDelete(User $user, Board $board)
    {
        //
    }
}
