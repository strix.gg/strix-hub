<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateHex implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $color = substr($value, 1);

        return ctype_xdigit($color) & strlen($color) == 6;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The color is not a valid hex.';
    }
}
