<?php

namespace App\Events\User\Credit;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Spatie\EventSourcing\ShouldBeStored;

class CreditsSubtracted implements ShouldBeStored
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var string */
    public $modelUuid;

    /** @var int */
    public $amount;

    /**
     * Create a new event instance.
     *
     * @param string $modelUuid
     * @param int $amount
     */
    public function __construct(string $modelUuid, int $amount)
    {
        $this->modelUuid = $modelUuid;

        $this->amount = $amount;
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
