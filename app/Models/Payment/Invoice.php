<?php

namespace App\Models\Payment;

use App\Traits\Models\AccessesRules;
use App\Traits\Models\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use GeneratesUuid, AccessesRules;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
    ];

    /**
     * Rules verifying that the data being stored matches the expectations of the database.
     *
     * @var array
     */
    public static $rules = [
    ];
}
