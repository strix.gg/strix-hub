<?php

namespace App\Models;

use App\Models\Forum\Thread;
use App\Traits\Colors\CachesColor;
use App\Traits\Colors\Colorable;
use App\Traits\Colors\Colorer;
use App\Traits\Comments\Commentable;
use App\Traits\Models\AccessesRules;
use App\Traits\Models\CachesMediaUrl;
use App\Traits\Models\GeneratesUuid;
use Cache;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Traits\Comments\Commenter;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $slug
 * @property string|null $tagline
 * @property int $reputation
 * @property int $stars
 * @property int $username_changes
 * @property int|null $credits
 * @property int|null $steam_account_id
 * @property \Illuminate\Support\Carbon|null $last_visited_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $userComments
 * @property-read int|null $user_comments_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCredits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastVisitedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereReputation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereStars($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSteamAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereTagline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsernameChanges($value)
 */
class User extends Authenticatable implements HasMedia
{
    use Notifiable, GeneratesUuid, AccessesRules, Commenter, Commentable, HasMediaTrait, HasRoles, CachesMediaUrl, Colorer, Colorable, CachesColor;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'slug', 'reputation', 'tagline', 'stars', 'username_changes', 'last_visited_at', 'steam_account_id', 'preferred_role_id'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'roles'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_visited_at' => 'datetime'
    ];

    /**
     * Rules verifying that the data being stored matches the expectations of the database.
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|min:1|max:120|unique:users,name',
        'email' => 'nullable|sometimes|email|max:255|unique:users,email',
        'password' => 'nullable|sometimes|max:255|confirmed',
        'current_password' => 'nullable|required_with:password|max:255',
        'tagline' => 'nullable|sometimes|string|max:255'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * Registers the media collections the user will have
     *
     */
    public function registerMediaCollections():void
    {
        $this->addMediaCollection('avatar')
            ->singleFile();

        $this->addMediaCollection('cover')
            ->singleFile();

        $this->addMediaCollection('postbit_cover')
            ->singleFile();
    }

    /**
     * Returns all threads user owns.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function threads()
    {
        return $this->hasMany(Thread::class);
    }

    /**
     * Returns the credit row the user owns.
     *
     * @return HasOne
     */
    public function credit(): HasOne
    {
        return $this->hasOne(Credit::class, 'user_id', 'steam_account_id');
    }

    /**
     * Returns the users preferred role for which userbar to display.
     *
     * @return Role
     */
    public function preferredRole(): Role
    {
        $role = Cache::remember(class_basename($this) . $this->id . 'preferred_role', now()->addMinute(), function () {
            if ($this->preferred_role_id === null) {
                return $this->roles->last();
            }

            return Role::whereId($this->preferred_role_id)->first();
        });

        return $role;
    }

    /**
     * What channel discord notifications go to.
     *
     * @return string
     */
    public function routeNotificationForDiscord(): string
    {
        return '641911411487801347';
    }

    /**
     * Returns if the user is a verified staff member.
     *
     * @return bool
     */
    public function isStaff(): bool
    {
        return \Cache::remember(class_basename($this) . $this->id . 'role', now()->addMinutes(5), function () {
            return $this->hasAnyRole(config('strix.staff_roles'));
        });
    }

    /**
     * Returns if the user is a premium member.
     *
     * @return bool
     */
    public function isPremium(): bool
    {
        return \Cache::remember(class_basename($this) . $this->id . 'premium_role', now()->addMinutes(5), function () {
            return $this->hasAnyRole(config('strix.premium_roles'));
        });
    }

    /**
     * Returns if the user is online or not.
     *
     * @return bool
     */
    public function isOnline(): bool
    {
        $online = false;

        $currentTime = now();

        $minuteThreshold = 15;

        $timeDifference = $currentTime->diffInMinutes($this->last_visited_at);

        if ($timeDifference <= $minuteThreshold) {
            $online = true;
        }

        if ($this->last_visited_at === null){
            $online = false;
        }

        return $online;
    }
}
