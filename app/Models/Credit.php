<?php

namespace App\Models;

use App\Events\User\Credit\CreditsAdded;
use App\Events\User\Credit\CreditsSubtracted;
use App\Notifications\Payment\Gateway\Paypal\IpnPaymentNotification;
use App\Traits\Models\AccessesRules;
use App\Traits\Models\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Credit extends Model
{
    use GeneratesUuid, AccessesRules, Notifiable;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'balance', 'user_id'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Rules verifying that the data being stored matches the expectations of the database.
     *
     * @var array
     */
    public static $rules = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'steam_account_id');
    }

    /**
     * @param int $amount
     */
    public function addCredits(int $amount)
    {
        \Event::dispatch(new CreditsAdded($this->id, $amount));
    }

    /**
     * @param int $amount
     */
    public function subtractCredits(int $amount)
    {
        \Event::dispatch(new CreditsSubtracted($this->id, $amount));
    }

    /**
     * @return string
     */
    public function routeNotificationForDiscord()
    {
        return '639209895949893678';
    }
}
