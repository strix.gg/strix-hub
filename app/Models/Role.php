<?php

namespace App\Models;

use App\Traits\Models\CachesMediaUrl;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Permission\Models\Role as RoleModel;

class Role extends RoleModel implements HasMedia
{
    use HasMediaTrait, CachesMediaUrl;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'permissions'
    ];

    /**
     * Media collection for userbars
     *
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('userbar')
            ->singleFile();
    }
}
