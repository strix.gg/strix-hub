<?php

namespace App\Models\Forum;

use App\Models\User;
use App\Traits\Comments\Commentable;
use App\Traits\Models\AccessesRules;
use App\Traits\Models\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Forum\Thread
 *
 * @property string $id
 * @property string $title
 * @property string $slug
 * @property int $comment_count
 * @property string $content
 * @property string $board_id
 * @property string $user_id
 * @property int $locked
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Forum\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \App\Models\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Thread onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereBoardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereCommentCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereLocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Thread whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Thread withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Thread withoutTrashed()
 * @mixin \Eloquent
 */
class Thread extends Model
{
    use GeneratesUuid, AccessesRules, Commentable, SoftDeletes;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'description', 'user_id', 'slug', 'board_id', 'uid', ''
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
    ];

    /**
     * Rules verifying that the data being stored matches the expectations of the database.
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string|min:1|max:100',
        'content' => 'required|json'
    ];

    public function getRouteKeyName(): string
    {
        return 'uid';
    }

    /**
     * Returns board that the thread belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function board(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Board::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
