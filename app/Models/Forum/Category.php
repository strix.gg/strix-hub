<?php

namespace App\Models\Forum;

use App\Traits\Models\AccessesRules;
use App\Traits\Models\CachesMediaUrl;
use App\Traits\Models\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * App\Models\Forum\Category
 *
 * @property string $id
 * @property string $title
 * @property string $slug
 * @property string|null $description
 * @property int $locked
 * @property int $private
 * @property int $weight
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Forum\Board[] $boards
 * @property-read int|null $boards_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Category onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereLocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category wherePrivate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Category whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Category withoutTrashed()
 * @mixin \Eloquent
 */
class Category extends Model implements HasMedia
{
    use GeneratesUuid, AccessesRules, HasMediaTrait, SoftDeletes, CachesMediaUrl;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'description'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'boards', 'boards.threads'
    ];

    /**
     * Rules verifying that the data being stored matches the expectations of the database.
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string|min:1|max:255',
        'description' => 'nullable|sometimes|string|min:1|max:255'
    ];

    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function boards(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Board::class);
    }

    /**
     *
     */
    public function registerMediaCollections()
    {
        $this->addMediaCollection('cover')
            ->singleFile();
    }
}
