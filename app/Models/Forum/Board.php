<?php

namespace App\Models\Forum;

use App\Traits\Models\AccessesRules;
use App\Traits\Models\CachesMediaUrl;
use App\Traits\Models\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * App\Models\Forum\Board
 *
 * @property string $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $category_id
 * @property int $locked
 * @property int $private
 * @property int $thread_count
 * @property int $comment_count
 * @property int $weight
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Forum\Board $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Forum\Thread[] $threads
 * @property-read int|null $threads_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Board onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereCommentCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereLocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board wherePrivate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereThreadCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Forum\Board whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Board withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Forum\Board withoutTrashed()
 * @mixin \Eloquent
 */
class Board extends Model implements HasMedia
{
    use GeneratesUuid, AccessesRules, HasMediaTrait, SoftDeletes, CachesMediaUrl;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'description', 'category_id'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
    ];

    /**
     * Rules verifying that the data being stored matches the expectations of the database.
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string|min:1|max:255',
        'description' => 'nullable|sometimes|string|min:1|max:255',
        'category_id' => 'required'
    ];

    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * Returns board that the category belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Returns all threads from this board.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function threads(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Thread::class);
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('icon')
            ->singleFile();
    }
}
