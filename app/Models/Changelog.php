<?php

namespace App\Models;

use App\Traits\Models\AccessesRules;
use App\Traits\Models\CachesMediaUrl;
use App\Traits\Models\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Changelog extends Model implements HasMedia
{
    use GeneratesUuid, AccessesRules, HasMediaTrait, CachesMediaUrl;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content'
    ];

    /**
     * Rules verifying that the data being stored matches the expectations of the database.
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string',
        'content' => 'required|json'
    ];
}
