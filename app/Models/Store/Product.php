<?php

namespace App\Models\Store;

use App\Traits\Models\AccessesRules;
use App\Traits\Models\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use GeneratesUuid, AccessesRules;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'base_amount', 'bonus_amount', 'price'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Rules verifying that the data being stored matches the expectations of the database.
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string|min:1|max:255',
        'base_amount' => 'required|int',
        'bonus_amount' => 'required|int',
        'price' => 'required|int'
    ];
}
