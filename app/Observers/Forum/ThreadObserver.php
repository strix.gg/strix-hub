<?php

namespace App\Observers\Forum;

use App\Models\Forum\Thread;

class ThreadObserver
{
    /**
     * Handle the thread "created" event.
     *
     * @param  \App\Models\Forum\Thread  $thread
     * @return void
     */
    public function created(Thread $thread)
    {
        //
    }

    /**
     * Handle the thread "updated" event.
     *
     * @param  \App\Models\Forum\Thread  $thread
     * @return void
     */
    public function updated(Thread $thread)
    {
        \Log::info($thread);

        if ($thread->isDirty('comment_count')){
            if ($thread->comment_count > $thread->getOriginal('comment_count')){
                $thread->board()->increment('comment_count', 1);
            } else {
                $thread->board()->decrement('comment_count', 1);
            }
        }
    }

    /**
     * Handle the thread "deleted" event.
     *
     * @param  \App\Models\Forum\Thread  $thread
     * @return void
     */
    public function deleted(Thread $thread)
    {
        //
    }

    /**
     * Handle the thread "restored" event.
     *
     * @param  \App\Models\Forum\Thread  $thread
     * @return void
     */
    public function restored(Thread $thread)
    {
        //
    }

    /**
     * Handle the thread "force deleted" event.
     *
     * @param  \App\Models\Forum\Thread  $thread
     * @return void
     */
    public function forceDeleted(Thread $thread)
    {
        //
    }
}
