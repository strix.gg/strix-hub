<?php

namespace App\Observers;

use App\Models\Comment;
use App\Models\Forum\Thread;

class CommentObserver
{
    /**
     * Handle the comment "created" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function created(Comment $comment)
    {
        if($comment->commentable_type === Thread::class) {
            $comment->commentable()->increment('comment_count');
        }
    }

    /**
     * Handle the comment "updated" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function updated(Comment $comment)
    {
        //
    }

    /**
     * Handle the comment "deleted" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function deleted(Comment $comment)
    {
        if($comment->commentable_type === Thread::class) {
            $comment->commentable->decrement('comment_count');
        }
    }

    /**
     * Handle the comment "restored" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function restored(Comment $comment)
    {
        if($comment->commentable_type === Thread::class) {
            $comment->commentable->increment('comment_count');
        }
    }

    /**
     * Handle the comment "force deleted" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function forceDeleted(Comment $comment)
    {
        if($comment->commentable_type === Thread::class) {
            $comment->commentable->decrement('comment_count');
        }
    }
}
