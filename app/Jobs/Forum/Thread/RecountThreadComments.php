<?php

namespace App\Jobs\Forum\Thread;

use App\Models\Forum\Thread;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RecountThreadComments implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $threads = Thread::all();

        foreach ($threads as $thread) {
            $commentCount = $thread->comments->count();

            $thread->comment_count = $commentCount;

            $thread->save();
        }
    }
}
