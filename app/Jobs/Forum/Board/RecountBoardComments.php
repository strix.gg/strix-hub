<?php

namespace App\Jobs\Forum\Board;

use App\Models\Forum\Board;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RecountBoardComments implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $boards = Board::all();

        foreach ($boards as $board){
            $threads = $board->threads;

            $commentCount = 0;

            foreach ($threads as $thread) {
                $commentCount += $thread->comments->count();
            }

            $board->comment_count = $commentCount;

            $board->save();
        }
    }
}
