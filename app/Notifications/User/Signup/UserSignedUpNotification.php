<?php

namespace App\Notifications\User\Signup;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Discord\DiscordChannel;
use NotificationChannels\Discord\DiscordMessage;

class UserSignedUpNotification extends Notification
{
    use Queueable;

    protected $user;

    protected $image;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     * @param string $image
     */
    public function __construct(User $user, string $image)
    {
        $this->user = $user;
        $this->image = $image;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [DiscordChannel::class];
    }

    public function toDiscord()
    {
        $embed = [
            'title' => 'User Signed up: ' . $this->user->name,
            'color' => 3051519,
            'thumbnail' => [
                'url' =>  $this->image
            ],
            'fields' => [
                [
                    'name' => 'Provider:',
                    'value' => 'Steam',
                    'inline' => true,
                ],
                [
                    'name' => 'Url:',
                    'value' => route('users.show', $this->user),
                    'inline' => true,
                ],
                [
                    'name' => 'Joined:',
                    'value' => now()->toDateString(),
                    'inline' => true,
                ],
                [
                    'name' => 'Steam Account ID:',
                    'value' => $this->user->steam_account_id,
                    'inline' => true,
                ],
            ]
        ];

        return DiscordMessage::create('' , $embed);
    }
}
