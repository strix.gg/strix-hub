<?php

namespace App\Notifications\Payment\Gateway\Paypal;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Discord\DiscordChannel;
use NotificationChannels\Discord\DiscordMessage;

class IpnDisputeNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [DiscordChannel::class];
    }

    public function toDiscord($notifiable)
    {
        $embed = [
            'title' => 'Atlas',
            'color' => 15023678,
            'thumbnail' => [
                'url' =>  'https://asapgaming.co/storage/avatars/88fc350b291e102f4eca59bedc804422.jpg'
            ],
            'fields' => [
                [
                    'name' => 'Package Name:',
                    'value' => 'Credits: 200',
                    'inline' => true,
                ],
                [
                    'name' => 'Amount Paid:',
                    'value' => '29.99',
                    'inline' => true,
                ],
                [
                    'name' => 'Current Credits:',
                    'value' => '0',
                    'inline' => true,
                ],
                [
                    'name' => 'Type:',
                    'value' => 'Chargeback / Dispute',
                    'inline' => true,
                ],
                [
                    'name' => 'Purchased Date:',
                    'value' => now()->toDateString(),
                    'inline' => true,
                ],

                [
                    'name' => 'Steam Account ID:',
                    'value' => '0',
                    'inline' => true,
                ],
            ]
        ];

        return DiscordMessage::create('' , $embed);
    }
}
