<?php


namespace App\Traits\Models;


trait CachesMediaUrl
{
    /**
     * Caches Media urls to avoid duplicate calls
     *
     * @param string $collectionName
     * @return mixed
     */
    public function getMediaUrl(string $collectionName): string
    {
        $media = \Cache::remember($this->getMediaCacheKey($collectionName), self::mediaExpiresIn(), function () use ($collectionName) {
            return url($this->getFirstMediaUrl($collectionName));
        });

        return $media;
    }

    /**
     * Forgets media url cache.
     *
     * @param string $collectionName
     * @return bool
     */
    public function flushMediaCache(string $collectionName): bool
    {
        return \Cache::forget($this->getMediaCacheKey($collectionName));
    }

    /**
     * Unique cache key to avoid overriding other cache keys using the trait.
     *
     * @param string $collectionName
     * @return string
     */
    protected function getMediaCacheKey(string $collectionName): string
    {
        return (string) class_basename($this) . $this->id . $collectionName;
    }

    /**
     * Sets TTL for remember function. Override in base class to change cache time
     *
     * @return \Illuminate\Support\Carbon
     */
    protected static function mediaExpiresIn(): \Illuminate\Support\Carbon
    {
        return now()->addMinutes(5);
    }
}
