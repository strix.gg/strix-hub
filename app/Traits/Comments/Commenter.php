<?php

namespace App\Traits\Comments;

/**
 * Add this trait to your User model so
 * that you can retrieve the comments for a user.
 */

trait Commenter
{
    /**
     * Returns all comments that this user has made.
     */
    public function userComments()
    {
        return $this->morphMany(\App\Models\Comment::class, 'commenter');
    }
}
