<?php


namespace App\Traits\Colors;


trait Colorer
{
    /**
     * Returns returns the color a user set for themselves.
     */
    public function userColor()
    {
        return $this->morphOne(\App\Models\Color::class, 'colorer');
    }
}
