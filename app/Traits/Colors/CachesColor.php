<?php


namespace App\Traits\Colors;


trait CachesColor
{
    /**
     * @return string
     */
    public function getColor(): string
    {
        return \Cache::remember($this->getColorCacheKey(), self::colorExpiresIn(), function () {
            if ($this->color()->exists()){
                return $this->color->color;
            } else {
                return '#E0E0E6';
            }
        });
    }

    /**
     * @return bool
     */
    public function flushColorCache(): bool
    {
        return \Cache::forget($this->getColorCacheKey());
    }

    /**
     * @return string
     */
    public function getColorCacheKey(): string
    {
        return class_basename($this) . $this->id . 'color';
    }

    /**
     * @return \Illuminate\Support\Carbon
     */
    public static function colorExpiresIn(): \Illuminate\Support\Carbon
    {
        return now()->addMinutes(5);
    }
}
