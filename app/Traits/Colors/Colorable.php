<?php


namespace App\Traits\Colors;


trait colorable
{
    /**
     * Returns the color for this model.
     */
    public function color()
    {
        return $this->morphOne(\App\Models\Color::class, 'colorable');
    }
}
