<?php


namespace App\Services\Payment\Invoice;


use App\Models\Payment\Invoice;

class StoreService
{
    public function handle(array $data)
    {

    }

    protected function prepareModel(): Invoice
    {
        $invoice = new Invoice();

        $invoice->fill();

        return $invoice;
    }
}
