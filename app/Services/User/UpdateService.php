<?php


namespace App\Services\User;


use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class UpdateService
{
    public function handle(array $data, User $user): User
    {
        $data = $this->updateSlug($data);

        $user->update($data);

        return $user;
    }

    protected function updateSlug(array $data): array
    {
        $username = \Arr::get($data, 'name');

        $slug = \Str::slug($username);

        $slugExists = User::whereSlug($slug)->exists();

        if ($slugExists === false) {

            if (\preg_match('([A-Za-z0-9\-\_]+)', $slug)){

                $data = \Arr::add($data, 'slug', $slug);
            }
        }

        return $data;
    }
}
