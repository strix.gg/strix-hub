<?php


namespace App\Services\Forum\Board;


use App\Models\Forum\Board;

class UpdateService
{
    /**
     * @param array $data
     * @param Board $board
     * @return Board
     * @throws \Exception
     */
    public function handle(array $data, Board $board): Board
    {
        $data = generate_slug($data, 'title', false);

        $board->update($data);

        return $board;
    }
}
