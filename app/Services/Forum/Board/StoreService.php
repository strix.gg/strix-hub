<?php


namespace App\Services\Forum\Board;


use App\Models\Forum\Board;

class StoreService
{
    /**
     * Handles creation of category model.
     *
     * @param array $data
     * @return Board
     * @throws \Exception
     */
    public function handle(array $data): Board
    {
        $data = generate_slug($data, 'title', false);

        $board = $this->prepareModel($data);

        $board->save();

        return $board;
    }

    public function prepareModel(array $data): Board
    {
        $board = new Board();

        $board->fill($data);

        return $board;
    }
}
