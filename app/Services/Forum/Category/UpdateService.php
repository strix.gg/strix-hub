<?php


namespace App\Services\Forum\Category;


use App\Models\Forum\Category;

class UpdateService
{
    /**
     * Handles creation of category model.
     *
     * @param array $data
     * @return Category
     * @throws \Exception
     */
    public function handle(array $data, Category $category): Category
    {
        $data = generate_slug($data, 'title', false);

        $category->update($data);

        return $category;
    }
}
