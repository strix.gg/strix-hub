<?php


namespace App\Services\Forum\Category;


use App\Models\Forum\Category;

class StoreService
{
    /**
     * Handles creation of category model.
     *
     * @param array $data
     * @return Category
     * @throws \Exception
     */
    public function handle(array $data): Category
    {
        $data = generate_slug($data, 'title', false);;

        $category = $this->prepareModel($data);

        $category->save();

        return $category;
    }

    /**
     * @param array $data
     * @return Category
     */
    public function prepareModel(array $data): Category
    {
        $category = new Category();

        $category->fill($data);

        return $category;
    }
}
