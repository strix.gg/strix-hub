<?php


namespace App\Services\Forum\Thread;


use App\Models\Forum\Board;
use App\Models\Forum\Thread;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class UpdateService
{
    /**
     * Creates new thread and attaches board, user, slug.
     *
     * @param array $data
     * @param Thread $thread
     * @param Authenticatable $auth
     * @return Model
     * @throws \Exception
     */
    public function handle(array $data, Thread $thread): Model
    {
        $data = generate_slug($data, 'title', false);

        $thread->update($data);

        return $thread;
    }
}
