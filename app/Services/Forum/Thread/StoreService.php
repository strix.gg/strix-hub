<?php


namespace App\Services\Forum\Thread;


use App\Models\Forum\Board;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class StoreService
{
    /**
     * Creates new thread and attaches board, user, slug.
     *
     * @param array $data
     * @param Board $board
     * @param Authenticatable $auth
     * @return Model
     * @throws \Exception
     */
    public function handle(array $data, Board $board, Authenticatable $auth): Model
    {
        $data = $this->attachUser($data, $auth);

        $data = generate_slug($data, 'title', false);

        $data = $this->attachUid($data);

        $thread = $board->threads()->create($data);

        return $thread;
    }

    /**
     * Creates a new array and attaches user.
     *
     * @param array $data
     * @param Authenticatable $auth
     * @return array
     */
    protected function attachUser(array $data, Authenticatable $auth): array
    {
        $data = \Arr::add($data, 'user_id', $auth->id);

        return $data;
    }

    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    protected function attachUid(array $data): array
    {
        $randomized = random_str(12);

        $data = \Arr::add($data, 'uid', $randomized);

        return $data;
    }
}
