<?php


namespace App\Services;


abstract class BaseStoreService
{
    abstract public function handle();

    abstract protected function prepareModel();
}
