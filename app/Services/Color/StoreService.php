<?php


namespace App\Services\Color;


use App\Models\Color;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class StoreService
{
    /**
     * Handles creation of comment and attachment of commenter's model.
     *
     * @param array $data
     * @param User $user
     * @return Model
     */
    public function handle(array $data, User $user): Model
    {
        $data = $this->attachUser($data, $user);

        $colorer = $this->prepareModel($data);

        $color = $user->color()->save($colorer);

        return $color;
    }

    /**
     * Overwrites data with actual commenter from auth instance.
     *
     * @param array $data
     * @param Authenticatable $auth
     * @return array
     */
    protected function attachUser(array $data, User $user): array
    {
        $data = \Arr::add($data, 'colorer_id', $user->id);

        $data = \Arr::add($data, 'colorer_type', User::class);

        return $data;
    }

    /**
     * Creates comment from new model instance.
     *
     * @param array $data
     * @return Color
     */
    public function prepareModel(array $data): Model
    {
        $comment = new Color();

        $comment->fill($data);

        return $comment;
    }
}
