<?php


namespace App\Services\Color;


use App\Models\Color;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class UpdateService
{
    /**
     * Handles creation of comment and attachment of commenter's model.
     *
     * @param array $data
     * @param Color $color
     * @return Model
     */
    public function handle(array $data, Color $color): Model
    {
        $color->update($data);

        return $color;
    }
}
