<?php


namespace App\Services\Changelog;


use App\Models\Changelog;
use Illuminate\Support\Arr;

class StoreService
{
    /**
     * @param array $data
     * @return Changelog
     */
    public function handle(array $data): Changelog
    {
        $changelog = $this->prepareModel($data);
        $changelog->save();

        return $changelog;
    }

    /**
     * @param array $data
     * @return Changelog
     */
    protected function prepareModel(array $data)
    {
        $changelog = new Changelog();

        $changelog->fill($data);

        return $changelog;
    }
}
