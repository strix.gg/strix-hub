<?php


namespace App\Services\Changelog;


use App\Models\Changelog;

class UpdateService
{
    /**
     * @param array $data
     * @param Changelog $changelog
     * @return Changelog
     */
    public function handle(array $data, Changelog $changelog): Changelog
    {
        $changelog->update($data);

        return $changelog;
    }
}
