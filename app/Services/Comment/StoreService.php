<?php


namespace App\Services\Comment;


use App\Models\Comment;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class StoreService
{
    /**
     * Handles creation of comment and attachment of commenter's model.
     *
     * @param array $data
     * @param Model $model
     * @param Authenticatable $auth
     * @return Model
     */
    public function handle(array $data, Model $model, Authenticatable $auth): Model
    {
        $data = $this->attachUser($data, $auth);

        $commenter = $this->prepareModel($data);

        $comment = $model->comments()->save($commenter);

        return $comment;
    }

    /**
     * Overwrites data with actual commenter from auth instance.
     *
     * @param array $data
     * @param Authenticatable $auth
     * @return array
     */
    protected function attachUser(array $data, Authenticatable $auth): array
    {
        $data = \Arr::add($data, 'commenter_id', $auth->id);

        $data = \Arr::add($data, 'commenter_type', 'App\Models\User');

        return $data;
    }

    /**
     * Creates comment from new model instance.
     *
     * @param array $data
     * @return Comment
     */
    public function prepareModel(array $data): Model
    {
        $comment = new Comment();

        $comment->fill($data);

        return $comment;
    }
}
