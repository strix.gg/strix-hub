<?php


namespace App\Services\Comment;


use App\Models\Comment;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class UpdateService
{
    /**
     * Handles creation of comment and attachment of commenter's model.
     *
     * @param array $data
     * @param Comment $comment
     * @return Model
     */
    public function handle(array $data, Comment $comment): Model
    {
        $comment->update($data);

        return $comment;
    }
}
