<?php


namespace App\Services\Credit;


use App\Models\User;

class StoreService
{
    public function handle(User $user)
    {
        $credit = $user->credit()->create();

        return $credit;
    }
}
