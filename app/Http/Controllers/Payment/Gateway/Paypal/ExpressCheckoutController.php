<?php

namespace App\Http\Controllers\Payment\Gateway\Paypal;

use App\Http\Controllers\Controller;
use App\Models\Store\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;

class ExpressCheckoutController extends Controller
{
    protected $provider;

    /**
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->provider = new ExpressCheckout();
    }


    /**
     * @param Request $request
     * @param Product $credit
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getExpressCheckout(Request $request, Product $credit)
    {

        $user = $request->user();

        $recurring = $request->get('mode') ?? false;

        $cart = $this->getCheckoutData($user, $credit, $recurring);

        try {
            $options = [
                'BRANDNAME' => 'KingsGaming',
                'LOGOIMG' => 'https://i.imgur.com/xzAmkdl.png',
                'CHANNELTYPE' => 'Merchant',
            ];

            $response = $this->provider->addOptions($options)->setExpressCheckout($cart, $recurring);

            return redirect($response['paypal_link']);

        } catch (\Exception $e) {
          // $invoice = $this->createInvoice($cart, 'Invalid');
           // session()->put(['code' => 'danger', 'message' => "Error processing PayPal payment for Order $invoice->id!"]);
        }
    }

    /**
     * Process payment on PayPal.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @param Product $credit
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function getExpressCheckoutSuccess(Request $request, User $user, Product $credit)
    {
        $recurring = $request->get('mode') ?? false;

        $token = $request->get('token');

        $PayerID = $request->get('PayerID');

        $cart = $this->getCheckoutData($user, $credit, $recurring);

        // Verify Express Checkout Token
        $response = $this->provider->getExpressCheckoutDetails($token);

        if (\in_array(\strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {

            if ($recurring === true) {

                $response = $this->provider->createMonthlySubscription($response['TOKEN'], 9.99, $cart['subscription_desc']);

                if (! empty($response['PROFILESTATUS']) && in_array($response['PROFILESTATUS'], ['ActiveProfile', 'PendingProfile'])) {
                    $status = 'Processed';
                } else {
                    $status = 'Invalid';
                }

            } else {

                $options = [
                    'BRANDNAME' => 'KingsGaming',
                    'LOGOIMG' => 'https://i.imgur.com/xzAmkdl.png',
                    'CHANNELTYPE' => 'Merchant',
                ];

                // Perform transaction on PayPal
                $payment_status = $this->provider->addOptions($options)->doExpressCheckoutPayment($cart, $token, $PayerID);

                $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];
            }

            return redirect()->route('index');
        }
    }

    /**
     * @param User $user
     * @param Product $credit
     * @param bool $recurring
     * @return array
     */
    protected function getCheckoutData(User $user, Product $credit, $recurring = false)
    {
        $data = [];

        //Invoice::all()->count()
        $order_id =  $credit->id . '|' . $user->id . '|' . now()->toDayDateTimeString(); // this doesnt matter, can just create an invoice early with the id then process it later down

        // currently dont use monthly subs atm

        if ($recurring === true) {
            $data['items'] = [
                [
                    'name'  => $credit->title,
                    'price' => ($credit->price / 100),
                    'qty'   => 1,
                ]
            ];

            $data['return_url'] = route('index');

            $data['subscription_desc'] = 'Monthly Subscription '.config('paypal.invoice_prefix').' #'.$order_id;

        } else {
            // do the base credit and then bonus credit for 2nd item
            $data['items'] = [
                [
                    'name'  => 'Credits: ' . ($credit->base_amount + $credit->bonus_amount),
                    'price' => ($credit->price / 100),
                    'qty'   => 1,
                ]
            ];

            $data['return_url'] = route('payment.gateway.paypal.express.success', [$user, $credit]);
        }

        $data['invoice_id'] = config('paypal.invoice_prefix').'_'.$order_id;

        $data['invoice_description'] = $order_id;
        $data['cancel_url'] = route('index');
        $total = 0;

        foreach ($data['items'] as $item) {
            $total += $item['price'] * $item['qty'];
        }

        $data['total'] = ($total);

        return $data;
    }

}
