<?php


namespace App\Http\Controllers\Payment\Gateway\Paypal;


use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;

class IpnHandler
{
    protected $provider;

    public function __construct()
    {
        $this->provider = new ExpressCheckout();
    }

    public function handle(Request $request)
    {

        $request->merge(['cmd' => '_notify-validate']);

        $post = $request->all();

        $response = (string) $this->provider->verifyIPN($post);

        \Log::info('paypal_post_dump', $post);

        \Log::info('paypal_response_dump __ ' . $response);

        if ($response === 'VERIFIED') {

            // probably need to use title instead
            //explode('|', $order_id); use this for title

            // use mc_gross, determine between the current values of Product::class

            // if array_key_exists('reason_code', $post)

            //


        }
    }

    protected function dispute()
    {
        //
    }

    protected function payment()
    {
        //
    }
}
