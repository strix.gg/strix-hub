<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\Users\UserMediaUpdateFormRequest;
use App\Models\User;
use App\Http\Controllers\Controller;

class UserMediaController extends Controller
{
    /**
     * @param UserMediaUpdateFormRequest $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function update(UserMediaUpdateFormRequest $request, User $user)
    {

        if ($request->hasFile('avatar')) {
            $user->addMedia($request->file('avatar'))->toMediaCollection('avatar');

            $user->flushMediaCache('avatar');
        }

        if ($request->hasFile('cover') & $user->isPremium()) {
            $user->addMedia($request->file('cover'))->toMediaCollection('cover');

            $user->flushMediaCache('cover');
        }

        if ($request->hasFile('postbit_cover') & $user->isPremium()) {
            $user->addMedia($request->file('postbit_cover'))->toMediaCollection('postbit_cover');

            $user->flushMediaCache('postbit_cover');
        }

        return redirect()->route('users.show', $user);
    }
}
