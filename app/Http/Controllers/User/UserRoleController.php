<?php


namespace App\Http\Controllers\User;


use App\Models\User;
use Illuminate\Http\Request;

class UserRoleController
{
    public function update(Request $request, User $user)
    {
        $roles = $user->roles;

        if ($roles->contains($request->get('role'))){

            $user->update([
                'preferred_role_id' => $request->get('role')
            ]);
        }

        \Cache::forget('user_role_'. $user->id);

        return redirect()->back()->withInput();
    }
}
