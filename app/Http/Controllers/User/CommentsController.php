<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\Comment\CommentStoreFormRequest;
use App\Http\Requests\Comment\CommentUpdateFormRequest;
use App\Models\Comment;
use App\Models\User;
use App\Services\Comment\StoreService;
use App\Http\Controllers\Controller;

class CommentsController extends Controller
{

    protected $commentStoreService;

    public function __construct(StoreService $commentStoreService)
    {
        $this->commentStoreService = $commentStoreService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CommentStoreFormRequest $request
     * @param User $user
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(CommentStoreFormRequest $request, User $user)
    {
        $auth = \Auth::user();

        $this->authorize('create', Comment::class);

        $filteredRequest =  Comment::filterRequest($request);

        $this->commentStoreService->handle($filteredRequest, $user, $auth);

        return redirect()->route('users.show', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @param \App\Models\Comment $comment
     * @return void
     */
    public function edit(User $user, Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CommentUpdateFormRequest $request
     * @param User $user
     * @param \App\Models\Comment $comment
     * @return void
     */
    public function update(CommentUpdateFormRequest $request, User $user, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @param \App\Models\Comment $comment
     * @return void
     */
    public function destroy(User $user, Comment $comment)
    {
        //
    }
}
