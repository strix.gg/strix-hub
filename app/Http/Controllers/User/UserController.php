<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\Users\UserUpdateFormRequest;
use App\Models\User;
use App\Services\User\UpdateService;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    protected $userUpdateService;

    public function __construct(UpdateService $userUpdateService)
    {
        $this->userUpdateService = $userUpdateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(User $user)
    {
        $cacheKey = (string) class_basename(User::class) . $user->id . $user->slug;

        $auth = \Auth::user();

        $this->authorize('view', [$auth, $user]);

        $comments = $user->comments()->orderByDesc('created_at')->get();

        $threads = \Cache::remember($cacheKey, now()->addMinutes(5), function () use ($user) {
            return $user->threads()->latest()->with(['user', 'user.color'])->take(3)->get();
        });

        return view('pages.user.profile.show', compact('user', 'comments', 'threads'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(User $user)
    {
        $auth = \Auth::user();

        $this->authorize('update', [$auth, $user]);

        return view('pages.user.profile.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UserUpdateFormRequest $request, User $user)
    {
        $auth = \Auth::user();

        $this->authorize('update', [$auth, $user]);

        $data = User::filterRequest($request);

        $this->userUpdateService->handle($data, $user);

        return redirect()->route('users.show', compact('user'));
    }
}
