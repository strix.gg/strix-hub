<?php


namespace App\Http\Controllers\User;


use App\Http\Requests\Users\UserColorRequest;
use App\Models\Color;
use App\Models\User;
use App\Services\Color\StoreService;
use App\Services\Color\UpdateService;

class UserColorController
{
    protected $colorStoreService;

    protected $colorUpdateService;

    public function __construct(StoreService $colorStoreService, UpdateService $colorUpdateService)
    {
        $this->colorStoreService = $colorStoreService;

        $this->colorUpdateService = $colorUpdateService;
    }

    public function store(UserColorRequest $request, User $user)
    {
        if ($user->isPremium()) {
            $data = Color::filterRequest($request);

            $this->colorStoreService->handle($data, $user);

            $user->flushColorCache();
        }

        return redirect()->route('users.show', $user);
    }

    public function update(UserColorRequest $request, User $user)
    {

        if ($user->isPremium()){
            $data = Color::filterRequest($request);

            $this->colorUpdateService->handle($data, $user->color);

            $user->flushColorCache();
        }

        return redirect()->route('users.show', $user);
    }
}
