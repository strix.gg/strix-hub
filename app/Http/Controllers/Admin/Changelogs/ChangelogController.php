<?php

namespace App\Http\Controllers\Admin\Changelogs;

use App\Http\Controllers\Controller;
use App\Http\Requests\Changelog\ChangelogFormRequest;
use App\Models\Changelog;
use App\Services\Changelog\StoreService;
use App\Services\Changelog\UpdateService;
use Illuminate\Http\Request;

class ChangelogController extends Controller
{
    protected $changelogStoreService;

    protected $changelogUpdateService;

    public function __construct(StoreService $changelogStoreService, UpdateService $changelogUpdateService)
    {
        $this->changelogStoreService = $changelogStoreService;

        $this->changelogUpdateService = $changelogUpdateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $changelogs = Changelog::paginate(15);

        return view('pages.admin.changelog.index', compact('changelogs'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Changelog  $changelog
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.changelog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChangelogFormRequest $request)
    {
        $data = Changelog::filterRequest($request);

        $changelog = $this->changelogStoreService->handle($data);

        \Artisan::call('sign:version');

        return redirect()->route('admin.changelog.show', $changelog);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Changelog  $changelog
     * @return \Illuminate\Http\Response
     */
    public function show(Changelog $changelog)
    {
        return view('pages.admin.changelog.show', compact('changelog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Changelog  $changelog
     * @return \Illuminate\Http\Response
     */
    public function update(ChangelogFormRequest $request, Changelog $changelog)
    {
        $data = Changelog::filterRequest($request);

        $this->changelogUpdateService->handle($data, $changelog);

        return redirect()->route('admin.changelog.show', $changelog);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Changelog  $changelog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Changelog $changelog)
    {
        //
    }
}
