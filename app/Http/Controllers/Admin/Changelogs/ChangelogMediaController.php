<?php


namespace App\Http\Controllers\Admin\Changelogs;


use App\Models\Changelog;
use Illuminate\Http\Request;

class ChangelogMediaController
{
    public function update(Request $request, Changelog $changelog){
        if ($request->hasFile('cover')){
            $changelog->addMedia($request->file('cover'))->toMediaCollection('cover');
        }

        return redirect()->back()->withInput();
    }
}
