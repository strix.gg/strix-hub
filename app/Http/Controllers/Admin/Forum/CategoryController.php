<?php

namespace App\Http\Controllers\Admin\Forum;

use App\Http\Controllers\Controller;
use App\Http\Requests\Forum\Category\CategoryStoreFormRequest;
use App\Http\Requests\Forum\Category\CategoryUpdateFormRequest;
use App\Models\Forum\Category;
use App\Services\Forum\Category\StoreService;
use App\Services\Forum\Category\UpdateService;

class CategoryController extends Controller
{
    protected $categoryStoreService;

    protected $categoryUpdateService;

    public function __construct(StoreService $categoryStoreService, UpdateService $categoryUpdateService)
    {
        $this->categoryStoreService = $categoryStoreService;

        $this->categoryUpdateService = $categoryUpdateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate(15);

        return view('pages.admin.forum.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.forum.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryStoreFormRequest $request
     * @return void
     * @throws \Exception
     */
    public function store(CategoryStoreFormRequest $request)
    {
        $data = Category::filterRequest($request);

        $this->categoryStoreService->handle($data);

        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Forum\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('pages.admin.forum.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Forum\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryUpdateFormRequest $request
     * @param \App\Models\Forum\Category $category
     * @return void
     * @throws \Exception
     */
    public function update(CategoryUpdateFormRequest $request, Category $category)
    {
        $data = Category::filterRequest($request);

        $this->categoryUpdateService->handle($data, $category);

        return redirect()->route('admin.category.show', $category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Forum\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
