<?php

namespace App\Http\Controllers\Admin\Forum;

use App\Http\Controllers\Controller;
use App\Http\Requests\Forum\Board\BoardMediaUpdateFormRequest;
use App\Models\Forum\Board;
use Illuminate\Http\Request;

class BoardMediaController extends Controller
{
    public function update(BoardMediaUpdateFormRequest $request, Board $board)
    {
        if ($request->hasFile('icon')) {
            $board->addMedia($request->file('icon'))->toMediaCollection('icon');

            $board->flushMediaCache('icon');
        }

        return redirect()->back()->withInput();
    }
}
