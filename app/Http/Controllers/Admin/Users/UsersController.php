<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UserUpdateFormRequest;
use App\Models\Role;
use App\Models\User;
use App\Services\User\UpdateService;

class UsersController extends Controller
{

    protected $userUpdateService;

    /**
     * UsersController constructor.
     * @param UpdateService $userUpdateService
     */
    public function __construct(UpdateService $userUpdateService)
    {
        $this->userUpdateService = $userUpdateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(): \Illuminate\View\View
    {
        $users = User::paginate(15);

        return view('pages.admin.user.index', compact('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $roles = Role::all();

        return view('pages.admin.user.show', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateFormRequest $request
     * @param \App\Models\User $user
     * @return void
     */
    public function update(UserUpdateFormRequest $request, User $user)
    {
        $data = User::filterRequest($request);

        $this->userUpdateService->handle($data, $user);

        return redirect()->back()->withErrors();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
