<?php


namespace App\Http\Controllers\Admin\Users;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class UsersRoleController
{
    public function update(Request $request, User $user)
    {
        $user->syncRoles($request->get('roles'));

        return back()->withInput();
    }
}
