<?php


namespace App\Http\Controllers\Admin\Roles;


use App\Models\Role;
use Illuminate\Http\Request;

class RoleMediaController
{
    /**
     * @param Request $request
     * @param Role $role
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function update(Request $request, Role $role)
    {
        if ($request->hasFile('userbar')){
            $role->addMedia($request->file('userbar'))->toMediaCollection('userbar');

            $role->flushMediaCache('userbar');
        }

        return redirect()->back()->withInput();
    }
}
