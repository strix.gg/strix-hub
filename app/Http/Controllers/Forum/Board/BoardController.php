<?php

namespace App\Http\Controllers\Forum\Board;

use App\Http\Controllers\Controller;
use App\Http\Requests\Forum\Board\BoardStoreFormRequest;
use App\Http\Requests\Forum\Board\BoardUpdateFormRequest;
use App\Models\Forum\Board;
use App\Models\Forum\Category;
use App\Services\Forum\Board\StoreService;
use App\Services\Forum\Board\UpdateService;
use Illuminate\Http\Request;

class BoardController extends Controller
{
    protected $boardStoreService;

    protected $boardUpdateService;

    public function __construct(StoreService $boardStoreService, UpdateService $boardUpdateService)
    {
        $this->boardStoreService = $boardStoreService;

        $this->boardUpdateService = $boardUpdateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('pages.forum.board.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BoardStoreFormRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(BoardStoreFormRequest $request)
    {
        $data = Board::filterRequest($request);

        $this->boardStoreService->handle($data);

        return redirect()->route('forums.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Forum\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function show(Board $board)
    {
        $threads = $board->threads()->orderByDesc('created_at')->get();

        return view('pages.forum.board.show', compact('board', 'threads'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Forum\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function edit(Board $board)
    {
        $user = \Auth::user();

        return view('pages.forum.board.edit', compact('board', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BoardUpdateFormRequest $request
     * @param \App\Models\Forum\Board $board
     * @return void
     */
    public function update(BoardUpdateFormRequest $request, Board $board)
    {
        $data = Board::filterRequest($request);

        $this->boardUpdateService->handle($data, $board);

        return redirect()->back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Forum\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function destroy(Board $board)
    {
        //
    }
}
