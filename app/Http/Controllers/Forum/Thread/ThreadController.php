<?php

namespace App\Http\Controllers\Forum\Thread;

use App\Http\Controllers\Controller;
use App\Http\Requests\Forum\Thread\ThreadStoreFormRequest;
use App\Models\Forum\Board;
use App\Models\Forum\Thread;
use App\Services\Forum\Thread\StoreService;
use App\Services\Forum\Thread\UpdateService;
use Illuminate\Http\Request;

class ThreadController extends Controller
{

    protected $threadStoreService;

    protected $threadUpdateService;

    public function __construct(StoreService $threadStoreService, UpdateService $threadUpdateService)
    {
        $this->threadStoreService = $threadStoreService;

        $this->threadUpdateService = $threadUpdateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Board $board
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Board $board)
    {
        $this->authorize('create', Thread::class);

        return view('pages.forum.thread.create', compact('board'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ThreadStoreFormRequest $request
     * @param Board $board
     * @return void
     * @throws \Exception
     */
    public function store(ThreadStoreFormRequest $request, Board $board)
    {
        $this->authorize('create', Thread::class);

        $auth = \Auth::user();

        $data = Thread::filterRequest($request);

        $thread = $this->threadStoreService->handle($data, $board, $auth);

        return redirect()->route('forums.thread.show', [
            $thread, $thread->slug
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Forum\Thread $thread
     * @param string|null $slug
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Thread $thread, string $slug = null)
    {
        $this->authorize('view', $thread);

        $comments = $thread->comments()->orderBy('created_at', 'asc')->paginate(19);

        return view('pages.forum.thread.show', compact('thread', 'slug', 'comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Forum\Thread $thread
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Thread $thread)
    {
        $this->authorize('update', $thread);

        return view('pages.forum.thread.edit', compact('thread'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ThreadStoreFormRequest $request
     * @param \App\Models\Forum\Thread $thread
     * @return void
     * @throws \Exception|\Illuminate\Auth\Access\AuthorizationException
     */
    public function update(ThreadStoreFormRequest $request, Thread $thread)
    {
        $this->authorize('update', $thread);

        $data = Thread::filterRequest($request);

        $thread = $this->threadUpdateService->handle($data, $thread);

        return redirect()->route('forums.thread.show', [
            $thread, $thread->slug
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Forum\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy(Thread $thread)
    {
        //
    }
}
