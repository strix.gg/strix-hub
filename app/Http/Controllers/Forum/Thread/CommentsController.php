<?php

namespace App\Http\Controllers\Forum\Thread;

use App\Http\Controllers\Controller;
use App\Http\Requests\Comment\CommentStoreFormRequest;
use App\Models\Comment;
use App\Models\Forum\Thread;
use App\Models\User;
use App\Services\Comment\StoreService;
use App\Services\Comment\UpdateService;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CommentsController extends Controller
{
    protected $commentStoreService;

    protected $commentUpdateService;

    /**
     * CommentsController constructor.
     * @param StoreService $commentStoreService
     * @param UpdateService $commentUpdateService
     */
    public function __construct(StoreService $commentStoreService, UpdateService $commentUpdateService)
    {
        $this->commentStoreService = $commentStoreService;
        $this->commentUpdateService = $commentUpdateService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CommentStoreFormRequest $request
     * @param Thread $thread
     * @param User $user
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(CommentStoreFormRequest $request, Thread $thread)
    {
        $auth = Auth::user();

        $this->authorize('create', Comment::class);

        $filteredRequest =  Comment::filterRequest($request);

        $this->commentStoreService->handle($filteredRequest, $thread, $auth);

        return redirect()->route('forums.thread.show', [
            $thread, $thread->slug
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Thread $thread
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Comment $comment)
    {
        $this->authorize('update', $comment);

        return view('pages.forum.thread.comment.edit', compact('comment'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param CommentStoreFormRequest $request
     * @param Comment $comment
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(CommentStoreFormRequest $request, Comment $comment)
    {
        $this->authorize('update', $comment);

        $data = Comment::filterRequest($request);

        $this->commentUpdateService->handle($data, $comment);

        return redirect()->route('forums.thread.show', [
            $comment->commentable, $comment->commentable->slug
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Comment $comment
     * @return Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
