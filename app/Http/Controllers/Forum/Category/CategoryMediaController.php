<?php

namespace App\Http\Controllers\Forum\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Forum\Category\CategoryMediaUpdateFormRequest;
use App\Models\Forum\Category;
use App\Models\User;
use Illuminate\Http\Request;

class CategoryMediaController extends Controller
{
    public function update(CategoryMediaUpdateFormRequest $request, Category $category)
    {
        if ($request->hasFile('cover')) {
            $category->addMedia($request->file('cover'))->toMediaCollection('cover');
        }

        return redirect()->route('forums.index');
    }
}
