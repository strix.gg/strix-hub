<?php

namespace App\Http\Controllers\Forum\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Forum\Category\CategoryStoreFormRequest;
use App\Http\Requests\Forum\Category\CategoryUpdateFormRequest;
use App\Models\Forum\Category;
use App\Services\Forum\Category\StoreService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryStoreService;

    public function __construct(StoreService $categoryStoreService)
    {
        $this->categoryStoreService = $categoryStoreService;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $user = \Auth::user();

        $this->authorize('create', Category::class);

        return view('pages.forum.category.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryStoreFormRequest $request
     * @return void
     * @throws \Exception
     */
    public function store(CategoryStoreFormRequest $request)
    {
        $this->authorize('create', Category::class);

        $data = Category::filterRequest($request);

        $this->categoryStoreService->handle($data);

        return redirect()->route('forums.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Forum\Category $category
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Category $category)
    {
        $user = \Auth::user();

        $this->authorize('update', $user);

        return view('pages.forum.category.edit', compact('category', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryUpdateFormRequest $request
     * @param \App\Models\Forum\Category $category
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(CategoryUpdateFormRequest $request, Category $category)
    {
        $user = $request->user();

        $this->authorize('update', [$user, $category]);

        return redirect()->back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Forum\Category $category
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Category $category)
    {
        $user = \Auth::user();

        $this->authorize('update', [$user, $category]);
    }
}
