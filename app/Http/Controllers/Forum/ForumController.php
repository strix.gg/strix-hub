<?php

namespace App\Http\Controllers\Forum;

use App\Http\Controllers\Controller;
use App\Models\Forum\Category;
use App\Models\Forum\Thread;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $categories = Category::orderBy('weight', 'asc')->get();

        $threads = Thread::latest()->take(5)->get();

        return view('pages.forum.index', compact('categories', 'threads'));
    }
}
