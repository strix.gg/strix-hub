<?php

namespace App\Http\Controllers\Redirects;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DiscordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        return redirect()->away(config('strix.discord_url'));
    }
}
