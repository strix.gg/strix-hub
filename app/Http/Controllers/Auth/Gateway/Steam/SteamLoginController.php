<?php

namespace App\Http\Controllers\Auth\Gateway\Steam;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use kanalumaddela\LaravelSteamLogin\Http\Controllers\AbstractSteamLoginController;
use kanalumaddela\LaravelSteamLogin\SteamUser;

class SteamLoginController extends AbstractSteamLoginController
{
    /**
     * {@inheritdoc}
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function authenticated(Request $request, SteamUser $steamUser)
    {
        // auth logic goes here
        // e.g. $user = User::where('steam_account_id', $steamUser->accountId)->first();

        $user = User::whereSteamAccountId($steamUser->accountId)->first();

        if (! $user) {
            $steamUser->getUserInfo();

            // probably should be moved to a gateway service at some point
            $user = User::create([
                'name' => $steamUser->name, // personaname
                'steam_account_id' => $steamUser->accountId, // 31-bit actual unique steam id
                'slug' => Str::slug($steamUser->name)
            ]);

            // create the wallet on login TODO: move this to its own service;
            $user->credit()->create();

            $user->addMediaFromUrl($steamUser->avatarLarge)
                ->toMediaCollection('avatar');
        }

        Auth::login($user, true);
    }
}
