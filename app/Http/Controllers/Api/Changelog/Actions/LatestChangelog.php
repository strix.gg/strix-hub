<?php


namespace App\Http\Controllers\Api\Changelog\Actions;


use App\Models\Changelog;

class LatestChangelog
{
    public function __invoke()
    {
        $changelog = Changelog::all()->last();

        return [
            'title' => $changelog->title,
            'content' => $changelog->content,
            'created_at' => $changelog->created_at->toDayDateTimeString(),
            'cover' => url($changelog->getMediaUrl('cover')),
        ];
    }
}
