<?php

namespace App\Http\Controllers\Api\Minecraft;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use xPaw\MinecraftPing;
use xPaw\MinecraftPingException;
use xPaw\MinecraftQuery;
use xPaw\MinecraftQueryException;

class ServerQueryController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    public function __invoke(Request $request)
    {
        $Query = new MinecraftQuery();

        try
        {
            $Query->Connect( 'play.strix.gg', 25565 );

            return $Query->GetInfo();
        }
        catch( MinecraftQueryException $e )
        {
            echo $e->getMessage();
        }
    }
}
