<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use App\Models\User;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return User::all();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return array
     */
    public function show(User $user)
    {
        return [
            'name' => $user->name,
            'slug' => $user->slug,
            'stars' => $user->stars,
            'steam_account_id' => $user->steam_account_id,
            'tagline' => $user->tagline,
            'username_changes' => $user->username_changes,
            'cover' => url($user->getMediaUrl('cover')),
            'avatar' => url($user->getMediaUrl('avatar')),
            'role' => $user->preferredRole()->name,
            'userbar' => url($user->preferredRole()->getMediaUrl('userbar'))
        ];
    }
}
