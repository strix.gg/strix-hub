<?php

namespace App\Http\Controllers\Api\Discord;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ServerQueryController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $serverId = env('DISCORD_SERVER_ID', '623762708327694346');


        $discordUrl = "https://discordapp.com/api/servers/" . $serverId . "/widget.json";

        $json = file_get_contents($discordUrl);

        $decoded = json_decode($json, true);


        return count($decoded['members']);
    }
}
