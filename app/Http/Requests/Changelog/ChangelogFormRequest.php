<?php

namespace App\Http\Requests\Changelog;

use App\Models\Changelog;
use Illuminate\Foundation\Http\FormRequest;

class ChangelogFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Changelog::getRules();
    }
}
