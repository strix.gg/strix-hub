<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class UserMediaUpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if ($this->user()->isPremium()) {
            return [
                'avatar' => 'nullable|sometimes|image',
                'cover' => 'nullable|sometimes|image',
                'postbit_cover' => 'nullable|sometimes|image',
            ];
        }



        return [
            'avatar' => 'nullable|sometimes|image|mimes:jpeg,png',
        ];
    }
}
