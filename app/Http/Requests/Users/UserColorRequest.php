<?php

namespace App\Http\Requests\Users;

use App\Rules\ValidateHex;
use Illuminate\Foundation\Http\FormRequest;

class UserColorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'color' => [
                'required',
                new ValidateHex()
            ]
        ];
    }
}
