<?php

namespace App\Http\Requests\Users;

use App\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class UserUpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = User::getRules();

        Arr::set($rules, 'email', [
            'nullable',
            'sometimes',
            'email',
            'max:255',
            Rule::unique('users', 'email')->ignore($this->user),
        ]);

        Arr::set($rules, 'name', [
            'required',
            'string',
            'min:1',
            'max:255',
            Rule::unique('users', 'name')->ignore($this->user),
        ]);

        return $rules;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->input('current_password') !== null) {
                if (! Hash::check($this->current_password, $this->user->password)) {
                    $validator->errors()->add('current_password', 'Your current password is incorrect.');
                }
            }
        });
    }
}
