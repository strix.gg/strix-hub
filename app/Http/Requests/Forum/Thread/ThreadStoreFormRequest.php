<?php

namespace App\Http\Requests\Forum\Thread;

use App\Models\Forum\Thread;
use Illuminate\Foundation\Http\FormRequest;

class ThreadStoreFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Thread::getRules();
    }
}
