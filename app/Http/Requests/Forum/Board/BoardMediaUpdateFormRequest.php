<?php

namespace App\Http\Requests\Forum\Board;

use Illuminate\Foundation\Http\FormRequest;

class BoardMediaUpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'icon' => 'nullable|sometimes|image|mimes:jpeg,png',
        ];
    }
}
