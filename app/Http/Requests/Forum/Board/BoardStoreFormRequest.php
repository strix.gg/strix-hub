<?php

namespace App\Http\Requests\Forum\Board;

use App\Models\Forum\Board;
use Illuminate\Foundation\Http\FormRequest;

class BoardStoreFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Board::getRules();
    }
}
