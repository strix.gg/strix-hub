<?php

namespace App\Http\Requests\Forum\Category;

use App\Models\Forum\Category;
use Illuminate\Foundation\Http\FormRequest;

class CategoryStoreFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Category::getRules();
    }
}
