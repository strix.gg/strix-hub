<?php

namespace App\Http\Middleware;

use Closure;

class LastVisited
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (\Auth::check()) {

            $user = \Auth::user();

            $currentTime = now();

            $user->update([
                'last_visited_at' => $currentTime
            ]);


        }

        return $next($request);
    }
}
