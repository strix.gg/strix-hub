<?php

namespace App\Http\Middleware;

use Closure;

class CheckIfAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()){
            if (auth()->user()->hasRole('owner')){
                return $next($request);
            }
        }

        return redirect()->away('https://www.youtube.com/watch?v=k4JvZXX1e58&feature=youtu.be');
    }
}
