<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SignVersion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sign:version';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates Unique String for the version signature';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        \Cache::forget('version_signature');

        $signature = \Cache::rememberForever('version_signature', function () {

            return randomize();
        });

        $this->info('Signature has been generated. Signature is:' . $signature);
    }
}
