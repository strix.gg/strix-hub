<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;

class SetupOwners extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:owners';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets a user\'s role by his name';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param User $user
     * @param Role $role
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();

        foreach ($users as $user){
            if ($user->steam_account_id === 108016087) {
                $user->syncRoles('owner');
            };
        }

        $this->info('Owners have been given the owner role.');
    }
}
