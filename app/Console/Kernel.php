<?php

namespace App\Console;

use App\Console\Commands\SetupOwners;
use App\Console\Commands\SignVersion;
use App\Jobs\Forum\Board\RecountBoardComments;
use App\Jobs\Forum\Board\RecountBoardThreads;
use App\Jobs\Forum\Thread\RecountThreadComments;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SetupOwners::class,
        SignVersion::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->job(new RecountBoardThreads())
            ->everyMinute()
            ->withoutOverlapping();

        $schedule->job(new RecountBoardComments())
            ->everyMinute()
            ->withoutOverlapping();

        $schedule->job(new RecountThreadComments())
            ->everyMinute()
            ->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
