<?php

if (! function_exists('randomize')) {
    /**
     * Generates a random string used for various simple randomness.
     *
     * @param int $min
     * @param int $max
     * @param int $limit
     * @return string
     * @throws Exception
     */
    function randomize(int $min = 10, int $max = 16, int $limit = null)
    {
        $randomized = \hash('sha256', \random_bytes(\rand($min, $max)));

        if ($limit !== null)
        {
            return (string) \Illuminate\Support\Str::limit($randomized, $limit, '');
        }

        return (string) $randomized;
    }
}

if (! function_exists('generate_slug')) {
    /**
     * @param array $data
     * @param string $key
     * @param bool $unqiue
     * @return array
     * @throws Exception
     */
    function generate_slug(array $data, string $key, bool $unqiue = false): array
    {
        $slug = \Str::slug(
            \Arr::get($data, $key)
        );

        if ($unqiue === true){
            $slug = randomize(8, 16, 20) . '-' . $slug;
        }

        $data = \Arr::add($data, 'slug', $slug);

        return $data;
    }
}


if (! function_exists('random_str')) {
    /**
     * Generate a random string.
     * @param int $length
     * @param string $keyspace
     * @return string
     * @throws Exception
     */
    function random_str(
        int $length = 64,
        string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ): string {
        if ($length < 1) {
            throw new \RangeException("Length must be a positive integer");
        }
        $pieces = [];
        $max = \mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces []= $keyspace[random_int(0, $max)];
        }
        return \implode('', $pieces);
    }
}
