<?php

namespace App\Repository;

use App\Models\User;
use LazyElePHPant\Repository\Repository;

class NameRepository extends Repository
{
    public function model()
    {
        return User::class;
    }
}
