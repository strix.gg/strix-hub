<?php

namespace App\Providers;

use App\Models\Comment;
use App\Models\Forum\Board;
use App\Models\Forum\Category;
use App\Models\Forum\Thread;
use App\Models\User;
use App\Observers\CommentObserver;
use App\Observers\Forum\BoardObserver;
use App\Observers\Forum\CategoryObserver;
use App\Observers\Forum\ThreadObserver;
use App\Observers\UserObserver;
use Cache;
use Igaster\LaravelTheme\Facades\Theme;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Laravel\Telescope\TelescopeServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(TelescopeServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootObservers();

        Paginator::defaultView('vendor.pagination.default');

        $this->setThemeCache();
    }

    protected function setThemeCache()
    {
        if ($this->app->environment() === 'production'){
            Theme::setSetting('cache-version', Cache::get('version_signature'));
        } else {
            Theme::setSetting('cache-version', Str::random(40));
        }
    }


    public function bootObservers(): void
    {
        User::observe(UserObserver::class);

        Comment::observe(CommentObserver::class);

        Category::observe(CategoryObserver::class);
        Board::observe(BoardObserver::class);
        Thread::observe(ThreadObserver::class);
    }
}
