<?php

namespace App\Providers;

use App\Models\Comment;
use App\Models\Forum\Board;
use App\Models\Forum\Category;
use App\Models\Forum\Thread;
use App\Models\User;
use App\Policies\Comment\CommentPolicy;
use App\Policies\Forum\Board\BoardPolicy;
use App\Policies\Forum\Category\CategoryPolicy;
use App\Policies\Forum\Thread\ThreadPolicy;
use App\Policies\User\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Board::class => BoardPolicy::class,
        Category::class => CategoryPolicy::class,
        Thread::class => ThreadPolicy::class,
        Comment::class => CommentPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
